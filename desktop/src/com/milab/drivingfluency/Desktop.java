package com.milab.drivingfluency;

import java.io.File;
import java.util.List;

import com.milab.drivingfluency.algorithm.FluencySensing;
import com.milab.drivingfluency.db.Persistance;
import com.milab.drivingfluency.model.AggregatedEvent;
import com.milab.drivingfluency.model.AlgorithmResult;
import com.milab.drivingfluency.model.DriveRef;
import com.milab.drivingfluency.model.DriveSamples;

public class Desktop {

	public static void main(String[] args) throws Exception {		
		File dbDirectory = new File("..", "drives");
		if (!dbDirectory.exists()) {
			dbDirectory.mkdir();
		}
		
		int sumAggEventsCount = 0;
		int sumNoneAggEventsCount = 0;
		int sumModerateAggEventsCount = 0;
		int sumExtremeAggEventsCount = 0;
		int sumExceedAggEventsCount = 0;
		
    	//initialize persistance
		Persistance.init(dbDirectory);
		
		TableFormatter driveFormatter = new TableFormatter(System.out);
		driveFormatter
			.addColumn("Id", 6)
			.addColumn("Samples count", 15)
//			.addColumn("Events raw", 13)
//			.addColumn("Exceed threshold", 20)
//			.addColumn("Extreme", 10)
//			.addColumn("Moderate", 10)
//			.addColumn("None", 10)
			.addColumn("Name", 30)
			.endColumns();
		
		//get list of all drives
		List<DriveRef> driveRefs = Persistance.getInstance().getAllDrives();
				
		int driveIndex = 0;
		for (DriveRef driveRef : driveRefs) {
			try {
				//load drive samples
				DriveSamples drive = Persistance.getInstance().loadDrive(driveRef);
				
				//run algorithm
				AlgorithmResult driveResults = FluencySensing.resultForDrive(drive);
				
				//save results
				Persistance.getInstance().saveAlgorithmResult(driveRef, driveResults);
				
				//count different raw events
//				int exceedThreshold = 0, extreme = 0, moderate = 0, none = 0;
//				for (RawEvent event : driveResults.getRawEvents()) {
//					switch (event.intensity) {
//					case EXCEED_THRESHOLD:
//						exceedThreshold++;
//						break;
//					case EXTREME:
//						extreme++;
//						break;
//					case MODERATE:
//						moderate++;
//						break;
//					case NONE:
//						none++;
//						break;
//					}
//				}
				
				driveFormatter.printHeader();
				
				//print drive line
				driveFormatter.printLine(
						driveIndex,
						drive.getAccelerometerSamples().size(), 
//						driveResults.getRawEvents().size(), 
//						exceedThreshold, extreme, moderate, none,
						driveRef.getName());
				
				TableFormatter resultFormatter = new TableFormatter(System.out);
				resultFormatter
					.addColumn("Event ID", 10)
					.addColumn("Time from start", 18)
					.addColumn("Duration", 10)
					.addColumn("Classification", 15)
					.addColumn("Intensity", 15)
					.endColumns();
				
				List<AggregatedEvent> events = driveResults.getAggregatedEvents();
				for (int i = 0; i < events.size(); i++) {
					AggregatedEvent event = events.get(i);
					resultFormatter.printLine(
							i+1, //id
							(event.time - events.get(0).time) / 1000.0f, //time
							event.duration / 1000.0f, //duration
							event.classification,
							event.intensity);
					
					switch (event.intensity) {
					case EXCEED_THRESHOLD:
						sumExceedAggEventsCount++;
						break;
					case EXTREME:
						sumExtremeAggEventsCount++;
						break;
					case MODERATE:
						sumModerateAggEventsCount++;
						break;
					case NONE:
						sumNoneAggEventsCount++;
						break;
					}
				}
				
				sumAggEventsCount += events.size();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
			finally {
				driveIndex++;
			}
		}
		
		System.out.print("\n**** SUMMARY ****\n");
		
		TableFormatter sumFormatter = new TableFormatter();
		sumFormatter
			.addColumn("Events count", 15)
			.addColumn("Exceed", 10)
			.addColumn("Extreme", 10)
			.addColumn("Moderate", 10)
			.addColumn("None", 10)
			.endColumns();
		
		sumFormatter.printLine(
				sumAggEventsCount, 
				sumExceedAggEventsCount,
				sumExtremeAggEventsCount,
				sumModerateAggEventsCount,
				sumNoneAggEventsCount
		);
	}
}