package com.milab.drivingfluency;

import java.io.PrintStream;
import java.util.ArrayList;

public class TableFormatter {
	private StringBuilder formatBuilder = new StringBuilder();
	private ArrayList<String> names = new ArrayList<String>();
	private String format;
	private PrintStream out;
	private int lineIndex = 0;
	
	public TableFormatter(PrintStream out) {
		this.out = out;
	}
	public TableFormatter() {
		this(System.out);
	}
	
	public TableFormatter addColumn(String name, int maxWidth) {
		formatBuilder.append("%-" + maxWidth + "s ");
		names.add(name);
		return this;
	}
	public TableFormatter endColumns() {
		formatBuilder.append('\n');
		format = formatBuilder.toString();
		return this;
	}
	public TableFormatter printHeader() {
		lineIndex = 1;
		out.printf('\n' + format, names.toArray());
		return this;
	}
	
	public TableFormatter printLine(Object... values) {
		if (lineIndex == 0) {
			printHeader();
			lineIndex++;
		}
		else if (lineIndex == 10) {
			lineIndex = 0;
		}
		else {
			lineIndex++;
		}
		
		out.printf(format, values);
		return this;
	}
	
}
