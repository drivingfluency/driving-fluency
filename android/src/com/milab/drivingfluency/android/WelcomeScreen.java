package com.milab.drivingfluency.android;

import com.milab.drivingfluency.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

public class WelcomeScreen extends Activity {
	
	private Handler uiHandler;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);	//Removes Application title bar
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
				WindowManager.LayoutParams.FLAG_FULLSCREEN);	//Removes Notification bar

		setContentView(R.layout.welcome);
		
		uiHandler = new Handler();
		uiHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				finish();
				Intent myIntent = new Intent(WelcomeScreen.this,MainActivity.class);
				startActivity(myIntent);
			}
		}, 2200);
	}
}