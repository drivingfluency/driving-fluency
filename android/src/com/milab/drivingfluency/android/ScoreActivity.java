package com.milab.drivingfluency.android;

import org.holoeverywhere.widget.TextView;
import org.holoeverywhere.widget.Toast;

import com.milab.drivingfluency.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class ScoreActivity extends Activity {

	private TextView scoreTextView;
	
	public final static String EXTRA_DRIVE_SCORE = "DRIVE_SCORE";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.score_screen);
		
		scoreTextView = (TextView)findViewById(R.id.score);
		
		//on done
		findViewById(R.id.doneButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onDoneTapped();
			}
		});
		
		Intent intent = getIntent();
		double score = intent.getDoubleExtra(EXTRA_DRIVE_SCORE, -1);
		if (score == -1) {
			Toast.makeText(this, "Drive score missing", Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		
		scoreTextView.setText(String.format("%1.2f", score));
	}
	
	private void onDoneTapped() {
		finish();
	}
	
}
