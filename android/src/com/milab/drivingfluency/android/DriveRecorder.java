package com.milab.drivingfluency.android;

import java.util.Date;

import org.holoeverywhere.widget.TextView;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.milab.drivingfluency.R;
import com.milab.drivingfluency.algorithm.FluencySensing;
import com.milab.drivingfluency.db.Persistance;
import com.milab.drivingfluency.db.Persistance.SampleType;
import com.milab.drivingfluency.db.Persistance.SamplesSaveContext;
import com.milab.drivingfluency.model.AggregatedEvent;
import com.milab.drivingfluency.model.AlgorithmResult;
import com.milab.drivingfluency.model.Sample;

public class DriveRecorder extends SherlockFragment
	implements FluencySensing.Listener {
	
	public interface Listener {
		public void onSaving(AlgorithmResult result, long startTime, long endTime) throws Exception;
		public void onDone() throws Exception;
	}
	private Listener listener;
	
	private boolean finishedExpectedly = false;

	private EventView eventView;
	private ViewGroup samplesContainer;
	
	private Button stopButton;
	
	private PowerManager.WakeLock wakeLock;
	private Handler uiHandler;
	private long startTime = -1;

	public static FluencySensing fluencySensing;
	
	public void setListener(Listener listener) {
		this.listener = listener;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View res = inflater.inflate(R.layout.drive_recorder, null);
 
		samplesContainer = (ViewGroup)res.findViewById(R.id.samplesContainer);
		stopButton = (Button)res.findViewById(R.id.stopButton);
		stopButton.setOnClickListener(
				new View.OnClickListener() {
					public void onClick(View v) {
						onStopTapped();
					}
				});
		return res;
	}

	/** Called when the activity is first created. */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		uiHandler = new Handler();
		Persistance.getInstance().makeTemporaryDrive();
		
		try {
			Persistance p = Persistance.getInstance();
			accelerometerSavingContext = p.startSavingSamples(p.getTemporaryDrive(), SampleType.ACCELEROMETER);
			magneticFieldSavingContext = p.startSavingSamples(p.getTemporaryDrive(), SampleType.MAGNETIC_FIELD);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		{ // acquire wake lock
			PowerManager pm = (PowerManager)getActivity().getSystemService(Context.POWER_SERVICE);
			wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK,
					"wakeLockTag");
			wakeLock.acquire();
		}

		{
			SensorManager sm = getSensorManager();
			
			// register for accelerometer sensor events
			Sensor accelerometerSensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			sm.registerListener(
					accelerometerEventListener, 
					accelerometerSensor,
					SensorManager.SENSOR_DELAY_GAME);

			// register to magnetic field sensor events
			Sensor magneticFieldSensor = sm.getDefaultSensor(Sensor.TYPE_ORIENTATION);
			sm.registerListener(
					magneticFieldEventListener, 
					magneticFieldSensor,
					SensorManager.SENSOR_DELAY_UI);
		}

		fluencySensing = new FluencySensing(this);
		
		eventView = new EventView(getActivity(), fluencySensing);
		samplesContainer.addView(eventView);
	}

	@Override
	public void onResume() {
		super.onResume();
		wakeLock.acquire();
	}

	@Override
	public void onPause() {
		super.onPause();
		wakeLock.release();
	}
	
	@Override
	public void onStop() {
		super.onStop();
		if (!finishedExpectedly) {
			prematureCleanup();
		}
	}
	
	private SamplesSaveContext accelerometerSavingContext;
	private SamplesSaveContext magneticFieldSavingContext;
	
	private long lastTimestamp = -1;
	
	private SensorEventListener accelerometerEventListener = new SensorEventListener() {
		@Override
		public void onSensorChanged(SensorEvent event) {
			onAccelerometerChanged(event);
		}
		
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {}
	};
	
	private SensorEventListener magneticFieldEventListener = new SensorEventListener() {
		@Override
		public void onSensorChanged(SensorEvent event) {
			onMagneticFieldChanged(event);
		}
		
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {}
	};
	
	private void onAccelerometerChanged(SensorEvent event) {
		Date date  = new Date();
		if (startTime == -1) {
			startTime = date.getTime();//event.timestamp / 1000000;
		}			
		
		//event.timestamp is in nanosecond and we work in milliseconds
		final long timestamp = date.getTime();//event.timestamp / 1000000;
		
		if (lastTimestamp != -1) {
			if (timestamp < lastTimestamp) {
				System.err.println("HAHAHEY");
			}
		}
		lastTimestamp = timestamp;
		
		Sample sample = new Sample(
				event.values[0], 
				event.values[1], 
				event.values[2], 
				timestamp);
		
		Persistance.getInstance().saveSample(accelerometerSavingContext, sample, SampleType.ACCELEROMETER);
		
		fluencySensing.onAccelerometerSample(sample);
	}

	private void onMagneticFieldChanged(SensorEvent event) {
		long timestamp = new Date().getTime();
		Sample sample = new Sample(
				event.values[0], 
				event.values[1], 
				event.values[2], 
				timestamp);
		Persistance.getInstance().saveSample(magneticFieldSavingContext, sample, SampleType.MAGNETIC_FIELD);
	
		fluencySensing.onMagneticSample(sample);
	}
	
	public void onAggregatedEvent(final AggregatedEvent event) {
		uiHandler.post(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(
						getActivity(),
						String.format(
								"(%s, %s, %s)",
								event.intensity.toString(),
								event.classification.toString(),
								event.duration / 1000.0f
						),
						Toast.LENGTH_SHORT).show();
			}
		});
	};
	
	 @Override
	 public void onMagneticFieldTurn(final AggregatedEvent event) {
	         uiHandler.post(new Runnable() {
	                 @Override
	                 public void run() {
	                         showEvent(event, Gravity.TOP|Gravity.CENTER_HORIZONTAL);
	                 }
	         });
	 }

	 private void showEvent(AggregatedEvent event, int gravity) {
	         Toast t = Toast.makeText(
	                         getActivity(),
	                         String.format(
	                                         "(%s, %s, %s)",
	                                         event.intensity.toString(),
	                                         event.classification.toString(),
	                                         event.duration / 1000.0f
	                         ),
	                         Toast.LENGTH_SHORT);
	         t.setGravity(gravity, 0, 50);
	         t.show();
	 }

	private SensorManager getSensorManager() {
		return (SensorManager)getActivity().getSystemService(Context.SENSOR_SERVICE);
	}
		
	public void onStopTapped() {
		long endTime = new Date().getTime();
		fluencySensing.stop(endTime);
		
		stopButton.setEnabled(false);
		cleanUp();
		
		// save the drive
		saveDriveAsync();
		
		finishedExpectedly = true;
		
		try {
			listener.onSaving(fluencySensing.getResult(), startTime, endTime);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void cleanUp() {
		if (wakeLock.isHeld()) {
			wakeLock.release();
		}
		getSensorManager().unregisterListener(accelerometerEventListener);
		getSensorManager().unregisterListener(magneticFieldEventListener);
	}
	
	private void prematureCleanup() {
		cleanUp();
		try {
			Persistance.getInstance().endSavingSamples(accelerometerSavingContext, SampleType.ACCELEROMETER);
			Persistance.getInstance().endSavingSamples(magneticFieldSavingContext, SampleType.MAGNETIC_FIELD);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveDriveAsync() {
		try {
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						Persistance p = Persistance.getInstance();
						
						//save samples to file system
						p.endSavingSamples(accelerometerSavingContext, SampleType.ACCELEROMETER);
						p.endSavingSamples(magneticFieldSavingContext, SampleType.MAGNETIC_FIELD);
						
						//save results
						p.saveAlgorithmResult(p.getTemporaryDrive(), fluencySensing.getResult());
						
						uiHandler.post(new Runnable() {
							@Override
							public void run() {
								try {
									listener.onDone();
								}
								catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			}).start();
					//launch another thread to send to server
//						new Thread(new Runnable() {
//							@Override
//							public void run() {
//								Server.sendDrive(
//										FluencyActivity.this, 
//										driveRef,
//										MainActivity.facebookUser);
//								
//								uiHandler.post(new Runnable() {
//									@Override
//									public void run() {
//										onDone.run();
//									}
//								});
//							}
//						}).start();
		} 
		catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(getActivity(), "Error while saving drive", Toast.LENGTH_LONG)
					.show();
		}
	}
}
