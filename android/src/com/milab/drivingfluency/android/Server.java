package com.milab.drivingfluency.android;

import java.io.File;
import java.io.FileReader;
import java.sql.Blob;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;

import com.facebook.model.GraphUser;
import com.milab.drivingfluency.db.Persistance;
import com.milab.drivingfluency.db.Persistance.SampleType;
import com.milab.drivingfluency.model.DriveRef;

public class Server {
	
	private static final String SERVER_URL = "http://drivingfluency.appspot.com/";
	
	public static void sendDrive(Context context, DriveRef driveRef, GraphUser facebookUser) {
		int TIMEOUT_MILLISEC = 10000;  // = 10 seconds
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams,
                                                  TIMEOUT_MILLISEC);
        HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
        HttpClient client = new DefaultHttpClient(httpParams);
        
        File driveFile = Persistance.getInstance().getSamplesFile(driveRef, SampleType.ACCELEROMETER);
        
        HttpPost request = new HttpPost(SERVER_URL);
        try {
        	request.setEntity(new FileEntity(driveFile, "UTF8"));
	        client.execute(request);
		} 
        catch (Exception e) {
        	e.printStackTrace();
        }
	}
	
}
