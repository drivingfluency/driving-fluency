package com.milab.drivingfluency.android;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.holoeverywhere.widget.FrameLayout;
import org.holoeverywhere.widget.TextView;

import com.milab.drivingfluency.R;
import com.milab.drivingfluency.model.AggregatedEvent;
import com.milab.drivingfluency.model.RawEvent;
import com.milab.drivingfluency.model.RawEvent.Intensity;

import android.content.Context;

public class DriveEditorListItem extends FrameLayout {

	private TextView classificationTextView;
	private TextView intensityTextView;
	
	private AggregatedEvent event;
	
	private static final Map<RawEvent.Intensity, String> intensityToString = new HashMap<RawEvent.Intensity, String>() {{
		put(Intensity.NONE, "None");
		put(Intensity.MODERATE, "Moderate");
		put(Intensity.EXCEED_THRESHOLD, "Exceed threshold");
		put(Intensity.EXTREME, "Extreme");
	}};
	
	public DriveEditorListItem(AggregatedEvent event, long driveStartTime, Context context) {
		super(context);
		this.event = event;
		
		inflate(context, R.layout.edited_events_list_items, this);
		
		intensityTextView = (TextView)findViewById(R.id.intensity);
		classificationTextView = (TextView)findViewById(R.id.classification);
		
		intensityTextView.setText(intensityToString.get(event.intensity));
		classificationTextView.setText(event.classification.toString());				
	}
	
	public AggregatedEvent getEvent() {
		return event;
	}
}
