package com.milab.drivingfluency.android;

import java.util.Date;
import java.util.List;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.widget.Toast;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.milab.drivingfluency.R;
import com.milab.drivingfluency.db.Persistance;
import com.milab.drivingfluency.model.AggregatedEvent;
import com.milab.drivingfluency.model.AlgorithmResult;
import com.milab.drivingfluency.model.DriveRef;

public class FluencyActivity extends SherlockFragmentActivity {
	
	private DriveRecorder driveRecorder;
	private Date startDate;
	
	private AlgorithmResult result;
	private long endTime;
	private long startTime;
	private int userScore;
	
	private volatile boolean doneSavingRecorded = false;
	private volatile boolean doneSavingUser = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fluency_panel);
				
		//set activity title
		getSherlock().getActionBar().setTitle(R.string.fluency_title);

		driveRecorder = new DriveRecorder();
		driveRecorder.setListener(new DriveRecorder.Listener() {
			@Override
			public void onSaving(AlgorithmResult result, long startTime, long endTime) throws Exception {
				onSavingRecording(result, startTime, endTime);
			}
			
			@Override
			public void onDone() throws Exception {
				onDoneRecording();
			}
		});
		
		getSupportFragmentManager()
			.beginTransaction()
				.add(R.id.container, driveRecorder)
			.commit();
		
		startDate = new Date();
	}
	
	@Override
	public void onBackPressed() {
		//show a dialog asking the user if he wants to lose the drive and exit
		AlertDialog dialog = new AlertDialog.Builder(this)
			.setTitle("Sure you wanna do that?")
			.setMessage("The drive will be lost")
			.setPositiveButton("OK", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					FluencyActivity.super.onBackPressed();
				}
			})
			.setNegativeButton("Cancel", null)
			.create();
		dialog.show();
	}
	
	private void onSavingRecording(AlgorithmResult newResult, long startTime, long endTime) {
		result = newResult;
		this.startTime = startTime;
		this.endTime = endTime;
		
		// launch drive editor
		DriveEditor editedEvents = new DriveEditor();
		editedEvents.setArgs(result.getAggregatedEvents(), startTime, new DriveEditor.Listener() {
			@Override
			public void onDone(List<AggregatedEvent> result, int userScore)
					throws Exception {
				onEventsEditorDone(result, userScore);
			}
		});
		
		//show score in the action bar
		// We We currently disable the score in the drive editor screen - for the pilot version.
		//getSherlock().getActionBar().setTitle(String.format("Score: %1.2f", result.getScore()));
		
		getSupportFragmentManager()
		.beginTransaction()
			.remove(driveRecorder)
			.add(R.id.container, editedEvents)
		.commit();
	}
	
	private void onDoneRecording() {
		doneSavingRecorded = true;
		finishIfAllDone();
	}
	
	private void onEventsEditorDone(List<AggregatedEvent> userResults, int userScore) throws Exception {
		this.userScore = userScore;
		DriveRef driveRef = Persistance.getInstance().getTemporaryDrive();
		Persistance.getInstance().saveUserAggregatedEvents(driveRef, userResults);
		doneSavingUser = true;
		finishIfAllDone();
	}
	
	private void finishIfAllDone() {
		if (doneSavingUser && doneSavingRecorded) {
			DriveRef ref = Persistance.getInstance().nameTemporaryDrive(
					String.format(
							"%s",
							Persistance.generateDateName(startDate)));
			
			try {
				Persistance.getInstance().saveDriveSummary(
					ref,
					result,
					endTime - startTime, 
					(int)result.getScore(), 
					userScore);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
			Toast.makeText(this, "Drive saved successfully", Toast.LENGTH_SHORT).show();
			
			Intent intent = new Intent(this, ScoreActivity.class);
			intent.putExtra(ScoreActivity.EXTRA_DRIVE_SCORE, result.getScore());
			startActivity(intent);
			finish();
		}
	}
	
}
