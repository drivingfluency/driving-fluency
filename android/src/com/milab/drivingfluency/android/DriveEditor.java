package com.milab.drivingfluency.android;

import java.util.LinkedList;
import java.util.List;

import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.Toast;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.app.SherlockFragment;
import com.milab.drivingfluency.R;
import com.milab.drivingfluency.model.AggregatedEvent;

public class DriveEditor extends SherlockFragment {

	public interface Listener {
		public void onDone(List<AggregatedEvent> result, int userScore) throws Exception;
	}
	
	private EditText userScoreTextView;
	private ListView driveEventListView;
	private Button doneButton;
	
	private List<AggregatedEvent> userResult;
	private long startTime;
	private Listener listener;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View ret = inflater.inflate(R.layout.drive_editor, null);
		driveEventListView = (ListView)ret.findViewById(R.id.drive_event_list);
		doneButton = (Button)ret.findViewById(R.id.done);
		userScoreTextView = (EditText)ret.findViewById(R.id.userScore);
		
		userScoreTextView.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					onDoneTap();
				}
				return true;
			}
		});
		
		doneButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onDoneTap();
			}
		});
		
		return ret;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		updateUI();
	}
	
	public void setArgs(List<AggregatedEvent> result, long startTime, Listener listener) {
		// copy all of the events.
		// the copy will be the edited events
		userResult = new LinkedList<AggregatedEvent>();
		for (AggregatedEvent event : result) {
			AggregatedEvent newEvent = new AggregatedEvent();
			newEvent.classification = event.classification;
			newEvent.duration = event.duration;
			newEvent.intensity = event.intensity;
			newEvent.time = event.time;
			newEvent.xRatio = event.xRatio;
			newEvent.yRatio = event.yRatio;
			userResult.add(newEvent);
		}
		
		this.startTime = startTime;
		
		this.listener = listener;
		if (getView() != null) {
			updateUI();
		}
	}
	
	private void updateUI() {
		List<AggregatedEvent> noStartEnd = userResult.subList(1, userResult.size()-1);
		driveEventListView.setAdapter(
				new ArrayAdapter<AggregatedEvent>(
						getActivity(), 
						R.layout.edited_events_list_items, 
						noStartEnd) {
					public View getView(int position, View convertView, ViewGroup parent) {
						return new DriveEditorListItem(
								getItem(position), // aggregated event 
								startTime, // start time
								getActivity()
						);
					}
				}
		);
	}
	
	public void onDoneTap() {
		int userScore;
		try {
			userScore = Integer.parseInt(userScoreTextView.getText().toString());
			if (userScore > 100) {
				throw new RuntimeException("Invalid score");
			}
		}
		catch (Exception e) {
			Toast.makeText(getActivity(), "Invalid score", Toast.LENGTH_LONG).show();
			return;
		}
		
		doneButton.setEnabled(false);
		
		try {
			// let the listener know we're done
			listener.onDone(userResult, userScore);
		}
		catch (Exception e) {
			Toast.makeText(getActivity(), "Encountered an error", Toast.LENGTH_LONG).show();
			e.printStackTrace();
			doneButton.setEnabled(true);
		}
		
	}

}
