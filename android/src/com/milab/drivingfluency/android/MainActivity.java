package com.milab.drivingfluency.android;

import java.io.File;

import org.holoeverywhere.app.Activity;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.milab.drivingfluency.R;
import com.milab.drivingfluency.db.Persistance;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private UiLifecycleHelper facebookHelper;
	public static GraphUser facebookUser;
	
	
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);    
                
        User.init();
        
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				onLoggedIn();
			}
		}, new IntentFilter(User.ACTION_LOGGED_IN));
        
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				onLoggedOut();
			}
		}, new IntentFilter(User.ACTION_LOGGED_OUT));
        
        //initialize facebook
        facebookHelper = new UiLifecycleHelper(this, new Session.StatusCallback() {
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				onFacebookStatusChanged(session, state, exception);
			}
		});
        facebookHelper.onCreate(savedInstanceState);
        
        try {
        	// Create DrivingFluency directory if it doesn't exist already
        	File dbDirectory = new File (Environment.getExternalStorageDirectory().getAbsolutePath(), "/DrivingFluency");
            
        	// Create persistance object
        	Persistance.init(dbDirectory);
        }
        catch (Exception e) {
        	e.printStackTrace();
        }
        
        // Bind buttons to actions
        findViewById(R.id.from_file_button).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				previousDrivesTapped();
			}
		});
        findViewById(R.id.from_sensor_button).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				startDriveTapped();
			}
		});
//        findViewById(R.id.login_button).setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View arg0) {
//				loginTapped();
//			}
//		});
    }
    
    /*
    @Override
    public void onBackPressed() 
    {
       super.onBackPressed();
       Intent goToMainActivity = new Intent(getApplicationContext(),MainActivity.class);
       
       goToMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // Will clear out your activity history stack till now
       startActivity(goToMainActivity);

     }
    */
    
    /**
     * Called when the facebook online status changes
     * @param session
     * @param state
     * @param exception
     */
    private void onFacebookStatusChanged(Session session, SessionState state, Exception exception) {
    	if (exception != null) {
    		exception.printStackTrace();
    		return;
    	}
    	
    	switch (state) {
    	case OPENED:
    		User.getInstance().loginAsync(session, this);
    		break;
    	case CLOSED:
    		User.getInstance().logout(session, this);
    		break;
    		
    	default: break;
    	}
    }
    
    public void previousDrivesTapped() {
		startActivity(new Intent(this, DriveListActivity.class));
    }
    
    private void startDriveTapped() {
    	startActivity(new Intent(this, FluencyActivity.class));
    }
    
//    private void loginTapped() {
//    	User.getInstance().loginAsync(MainActivity.this);
//    }
    
    private void onLoggedIn() {
    	Toast.makeText(
    			this,
    			String.format("Hello %s", User.getInstance().getName()),
    			Toast.LENGTH_LONG).show();
    }
    
    private void onLoggedOut() {
    	Toast.makeText(
    			this,
    			"Logged out",
    			Toast.LENGTH_LONG).show();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	facebookHelper.onResume();
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	facebookHelper.onPause();
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	facebookHelper.onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	facebookHelper.onDestroy();
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	facebookHelper.onSaveInstanceState(outState);
    }
    
}