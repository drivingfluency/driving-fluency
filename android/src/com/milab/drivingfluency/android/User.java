package com.milab.drivingfluency.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;

public class User {

	public static final String ACTION_LOGGED_IN = "logged_in";
	public static final String ACTION_LOGGED_OUT = "logged_out";

	private static User user;

	private GraphUser facebookUser;

	private enum OnlineState {
		LOGGED_IN, LOGGED_OUT, LOGGING_IN
	}

	private OnlineState onlineState = OnlineState.LOGGED_OUT;

	public static void init() {
		if (user == null) {
			user = new User();
		}
	}

	public static User getInstance() {
		return user;
	}

	private User() {
	}

	public String getName() {
		return facebookUser.getName();
	}

	public void loginAsync(Session session, final Activity activity) {
		setOnlineState(OnlineState.LOGGING_IN, activity);
		Request.newMeRequest(session, new Request.GraphUserCallback() {
			@Override
			public void onCompleted(final GraphUser user, Response response) {
				if (user == null) {
					return;
				}

				facebookUser = user;

				setOnlineState(OnlineState.LOGGED_IN, activity);
			}
		}).executeAsync();
	}

	public void logout(Session session, Context context) {
		facebookUser = null;
	}

	private void setOnlineState(OnlineState state, Context context) {
		if (onlineState == state) {
			return;
		}

		OnlineState previous = onlineState;
		onlineState = state;

		if (previous == OnlineState.LOGGING_IN
				&& state == OnlineState.LOGGED_IN) {
			Intent intent = new Intent(ACTION_LOGGED_IN);
			LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
		} else if (state == OnlineState.LOGGED_OUT) {
			Intent intent = new Intent(ACTION_LOGGED_OUT);
			LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
		}
	}

}
