package com.milab.drivingfluency.android;

import com.milab.drivingfluency.algorithm.FluencySensing;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposePathEffect;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.PathEffect;
import android.graphics.RectF;
import android.view.KeyEvent;
import android.view.View;

public class EventView extends View {
	private Paint mPaint = new Paint();
	private RectF bounds = new RectF();
	private Path mPath;
	private PathEffect[] mEffects;
	private float mPhase;
	private FluencySensing ratioProvider;
	
	public EventView(Context context, FluencySensing ratioProvider) {
		super(context);
		
		this.ratioProvider = ratioProvider;
		
		setFocusable(true);
		setFocusableInTouchMode(true);
		mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeWidth(6);
		mPath = makeFollowPath();
		mEffects = new PathEffect[6];
	}
	
	private static void makeEffects(PathEffect[] e, float phase) {
		e[0] = null; // no effect
		e[1] = new CornerPathEffect(10);
		e[2] = new DashPathEffect(new float[] { 10, 5, 5, 5 }, phase);
		e[3] = new PathDashPathEffect(makePathDash(), 12, phase,
				PathDashPathEffect.Style.ROTATE);
		e[4] = new ComposePathEffect(e[2], e[1]);
		e[5] = new ComposePathEffect(e[3], e[1]);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(Color.WHITE);
		
		mPath.computeBounds(bounds, false);
		canvas.translate(10 - bounds.left, 10 - bounds.top);
		makeEffects(mEffects, mPhase);
		mPhase += 1;
		invalidate();
		int width = getWidth();
		int height = getHeight();
		mPaint.setStyle(Paint.Style.FILL);
		mPaint.setStrokeWidth(2);

		canvas.drawLine(0, height / 2, width, height / 2, mPaint);
		canvas.drawLine(width / 2, 0, width / 2, height, mPaint);
		mPaint.setColor(Color.GREEN);
		mPaint.setStyle(Paint.Style.STROKE);
		canvas.drawCircle(width / 2, height / 2, height / 4, mPaint);

		double xOffset = ratioProvider.getXRatio();
		double yOffset = ratioProvider.getYRatio();

		double redRatio = Math.sqrt(xOffset * xOffset + yOffset * yOffset);

		if (redRatio > 1) {
			xOffset = xOffset / redRatio;
			yOffset = yOffset / redRatio;

			redRatio = 1;
		}

		xOffset = xOffset * height / 4;
		yOffset = yOffset * height / 4;

		int green = 255;
		int red = 0;

		if (redRatio > 0.5) {
			green = (int) ((1 - redRatio) * 255);
			red = (int) (redRatio * 255);
		}

		mPaint.setColor(0xff001100 | (green << 8) | (red << 16));// redRatio*
																	// 0xffff0000
																	// );
		mPaint.setStyle(Paint.Style.FILL);

		canvas.drawCircle(width / 2 - (float) (xOffset), height / 2
				+ (float) yOffset, height / 22, mPaint);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_CENTER:
			mPath = makeFollowPath();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private static Path makeFollowPath() {
		Path p = new Path();
		p.moveTo(0, 0);
		for (int i = 1; i <= 15; i++) {
			p.lineTo(i * 20, (float) Math.random() * 35);
		}
		return p;
	}

	private static Path makePathDash() {
		Path p = new Path();
		p.moveTo(4, 0);
		p.lineTo(0, -4);
		p.lineTo(8, -4);
		p.lineTo(12, 0);
		p.lineTo(8, 4);
		p.lineTo(0, 4);
		return p;
	}
}
