package com.milab.drivingfluency.android;

import com.milab.drivingfluency.R;


import android.content.Context;
import android.widget.FrameLayout;
import android.widget.TextView;

public class DriveListItemView extends FrameLayout {
	
	private String driveName;
	private TextView idTextView;
	
	public DriveListItemView(Context context) {
		this(context, null);
	}
	
	public DriveListItemView(Context context, String driveName) {
		super(context);
		inflate(context, R.layout.drive_list_item, this);
		
		idTextView = (TextView)findViewById(R.id.idTextView);
		if (driveName != null) 
			setDriveName(driveName);
		
	}
	
	public void setDriveName(String driveName) {
		this.driveName = driveName;
		idTextView.setText(driveName);
	}
	
}
