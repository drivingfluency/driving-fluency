package com.milab.drivingfluency.android;

import java.util.List;

import com.actionbarsherlock.app.SherlockActivity;
import com.milab.drivingfluency.R;
import com.milab.drivingfluency.android.results.ResultsActivity;
import com.milab.drivingfluency.db.Persistance;
import com.milab.drivingfluency.model.DriveRef;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class DriveListActivity extends SherlockActivity {
	
	public static final String EXTRA_DRIVE_NAME = "drive name";
	
	private List<DriveRef> driveRefs;
	private ListView listView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drive_list);

		//get reference to list view
		listView = (ListView)findViewById(R.id.listView);
		
		//get all drives
		driveRefs = Persistance.getInstance().getAllDrives();
		
		//set activity title
		getSherlock().getActionBar().setTitle(
				String.format(getString(R.string.drive_list_title_format),
				driveRefs.size()
		));
		
		listView.setAdapter(new ArrayAdapter<DriveRef>(this, R.layout.drive_list_item, driveRefs) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				int index = driveRefs.size() - position - 1;
				return new DriveListItemView(getContext(), getItem(index).getName());
			}
		});
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				itemSelectedAtIndex(driveRefs.size() - position - 1);
			}
		});
	}
	
	void itemSelectedAtIndex(int index) {
		Intent intent = new Intent(this, ResultsActivity.class);
    	intent.putExtra(ResultsActivity.EXTRA_DRIVE_NAME, driveRefs.get(index).getName());
    	
    	startActivity(intent);
	}
	
}
