package com.milab.drivingfluency.android.results;

import com.actionbarsherlock.app.SherlockFragment;
import com.milab.drivingfluency.model.AlgorithmResult;

public class ResultsFragment extends SherlockFragment {
	
	private AlgorithmResult results;
		
	public void setAlgorithmResults(AlgorithmResult r) {
		this.results = r;
	}
	
	protected AlgorithmResult getAlgorithmResults() {
		return results;
	}
	
}
