package com.milab.drivingfluency.android.results;

import java.util.LinkedList;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries.GraphViewStyle;
import com.milab.drivingfluency.R;
import com.milab.drivingfluency.model.AggregatedEvent;
import com.milab.drivingfluency.model.AlgorithmResult;
import com.milab.drivingfluency.model.RawEvent;

public class GraphFragment extends ResultsFragment {

	public static final String ARG_DRIVE_REF = "drive_ref";
		
	private ViewGroup graphContainer;
	private LineGraphView graphView;
	
	static private int color = Color.GREEN;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.results_graph, null);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		try {
			graphContainer = (ViewGroup)getView().findViewById(R.id.graph_container);
						
			graphView = new LineGraphView(getActivity(), "");
			
			//graphView.setDrawBackground(true);
			graphView.setViewPort(0, 5);
			graphView.setScrollable(true);
			
			graphView.setVerticalLabels(new String[] {
					RawEvent.Intensity.EXCEED_THRESHOLD.toString(),
					RawEvent.Intensity.EXTREME.toString(),
					RawEvent.Intensity.MODERATE.toString(),
					RawEvent.Intensity.NONE.toString()
			});
			
			graphContainer.addView(graphView);
			
			AlgorithmResult result = getAlgorithmResults();			
			GraphViewData[] data = createGraphViewData(result);
			GraphViewSeries series = new GraphViewSeries(
					"Drive",
					new GraphViewStyle(color, 2),
					data
			);
			graphView.addSeries(series);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static GraphViewData[] createGraphViewData(AlgorithmResult result) {
		List<AggregatedEvent> events = result.getAggregatedEvents();
		final long firstEventTime = result.getAggregatedEvents().get(0).time;
				
		List<GraphViewData> l = new LinkedList<GraphViewData>();		
		for (AggregatedEvent e : events) {
			double x = (double)(e.time - firstEventTime) / 1000;
			double y = e.intensity.ordinal();
			
			GraphViewData nextEventStart = new GraphViewData(x,y);
			GraphViewData nextEventEnd = new GraphViewData(
					x + e.duration / 1000, 
					y
			);
			
			l.add(new GraphViewData(nextEventStart.valueX, 0));
			l.add(nextEventStart);
			l.add(nextEventEnd);
			l.add(new GraphViewData(nextEventEnd.valueX, 0));
		}
				
		GraphViewData[] arr = new GraphViewData[l.size()];
		int i = 0;
		for (GraphViewData data : l) {
			arr[i] = data;
			i++;
		}
				
		return arr;
	}

	
}
