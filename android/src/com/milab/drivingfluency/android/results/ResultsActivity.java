package com.milab.drivingfluency.android.results;

import org.holoeverywhere.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.milab.drivingfluency.R;
import com.milab.drivingfluency.algorithm.FluencySensing;
import com.milab.drivingfluency.db.Persistance;
import com.milab.drivingfluency.model.AlgorithmResult;
import com.milab.drivingfluency.model.DriveRef;
import com.milab.drivingfluency.model.DriveSamples;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.TabHost.OnTabChangeListener;

public class ResultsActivity extends SherlockFragmentActivity {

	public static final String EXTRA_DRIVE_NAME = "drive name";
	
	private DriveRef driveRef;
	private AlgorithmResult results;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results);
		
		String driveName = getIntent().getExtras().getString(EXTRA_DRIVE_NAME);
		driveRef = new DriveRef(driveName);
		
		try {
			DriveSamples samples = Persistance.getInstance().loadDrive(driveRef);
			results = FluencySensing.resultForDrive(samples);
		} 
		catch (Exception e) {
			Toast.makeText(this, "An error has occured", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
		
		Toast.makeText(this, String.format("Score: %1.2f", results.getScore()), Toast.LENGTH_LONG).show();
		
		//set action bar buttons
        ActionBar actionBar = getSupportActionBar();
        
        //disable action bar title,logo,home so that the top part doesn't show
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        
        //tell the action bar that we would like to navigate via tabs
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
        OnTabChangeListener listener = new OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {}
		};
        
        //table tab
        actionBar.addTab(
        	actionBar.newTab()
	        		.setText("Table")
	        		.setTabListener(new TabListener(TableFragment.class, listener)
        		));
        
        //graph tab     
        actionBar.addTab(
        	actionBar.newTab()
	        		.setText("Graph")
	        		.setTabListener(new TabListener(GraphFragment.class, listener)
        		));
	}
	
	/**
	 * Default implementation for a tab listener.
	 * Takes a class as a constructor argument to create instances of the class
	 */
	private class TabListener implements ActionBar.TabListener {
		Class<? extends ResultsFragment> fragmentClass;
		OnTabChangeListener onTabChangeListener;
		
		public TabListener(Class<? extends ResultsFragment> fragmentClass, OnTabChangeListener onTabChangeListener) {
			this.fragmentClass = fragmentClass;
			this.onTabChangeListener = onTabChangeListener;
		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			try {
				onTabChangeListener.onTabChanged(tab.getText().toString());
				ResultsFragment frag = (ResultsFragment)getSupportFragmentManager().findFragmentByTag(tab.getText().toString());
				
				if (frag == null) {
					frag = fragmentClass.newInstance();
					frag.setAlgorithmResults(results);
					ft.add(R.id.fragment_container, frag, tab.getText().toString());
				} else {
					ft.attach(frag);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			
			//Fragment frag = fragments.get(tab.getText().toString());
			Fragment frag = getSupportFragmentManager().findFragmentByTag(tab.getText().toString());
			if (frag != null)
				ft.detach(frag);
		}
	}
	
}
