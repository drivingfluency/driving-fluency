package com.milab.drivingfluency.android.results;

import org.holoeverywhere.widget.ListView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.milab.drivingfluency.R;
import com.milab.drivingfluency.model.AggregatedEvent;

public class TableFragment extends ResultsFragment {

	private ListView listView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.results_table, null);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		listView = (ListView)getView().findViewById(R.id.listView);
		
		final long startTime = getAlgorithmResults().getAggregatedEvents().get(0).time;
		
		listView.setAdapter(
                new ArrayAdapter<AggregatedEvent>(
                    getActivity(), 
                    R.layout.results_table_list_item,
                    getAlgorithmResults().getAggregatedEvents()) {
		        @Override
		        public View getView(int position, View convertView, ViewGroup parent) {
		                return new TableListItemView(
		                		getItem(position), 
		                		startTime, 
		                		position, 
		                		getActivity()
		                );
		        }
		});
	}
	
}
