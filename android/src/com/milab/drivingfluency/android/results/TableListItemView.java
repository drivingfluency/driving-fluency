package com.milab.drivingfluency.android.results;

import org.holoeverywhere.widget.FrameLayout;
import org.holoeverywhere.widget.TextView;

import com.milab.drivingfluency.R;
import com.milab.drivingfluency.model.AggregatedEvent;

import android.content.Context;

public class TableListItemView extends FrameLayout {

	private TextView idTextView;
	private TextView typeTextView;
	private TextView intensityTextView;
	private TextView durationTextView;
	private TextView timeTextView;
	
	private AggregatedEvent event;
	private long id;
	private long driveStartTime;
	
	public TableListItemView(AggregatedEvent event, long driveStartTime, long id, Context context) {
		super(context);
		this.event = event;
		this.driveStartTime = driveStartTime;
		this.id = id;
		
		inflate(context, R.layout.results_table_list_item, this);
		
		idTextView = (TextView)findViewById(R.id.idLabel);
		typeTextView = (TextView)findViewById(R.id.typeLabel);
		intensityTextView = (TextView)findViewById(R.id.intensityLabel);
		durationTextView = (TextView)findViewById(R.id.durationLabel);
		timeTextView = (TextView)findViewById(R.id.timeLabel);
		
		updateUI();
	}

	private void updateUI() {
		idTextView.setText(Long.toString(id));
		typeTextView.setText(event.classification.toString());
		intensityTextView.setText(event.intensity.toString());
		durationTextView.setText(Float.toString(event.duration / 1000.0f));
		timeTextView.setText(Float.toString((event.time - driveStartTime) / 1000.0f));
	}
	
}
