package com.milab.drivingfluency.model;

import com.milab.drivingfluency.algorithm.Classification;

public class AggregatedEvent extends RawEvent {
	
	public Classification classification;

	public AggregatedEvent() {}
	
	public AggregatedEvent(RawEvent event) {
		super(event);
	}
	
	public AggregatedEvent(RawEvent event, Classification c) {
		super(event);
		this.classification = c;
	}
	
	public AggregatedEvent(Intensity p, Classification c, double xR, double yR) {
		super(p, xR, yR);
		this.classification = c;
	}
	
	public AggregatedEvent(Intensity p, Classification c, double xR, double yR, long t) {
		super(p, xR, yR, t);
		this.classification = c;
	}

}
