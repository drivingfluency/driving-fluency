package com.milab.drivingfluency.model;

public class DriveRef {

	private String name;
	private DriveSamples drive;
	
	public DriveRef(String name) {
		this(null, name);
	}
	public DriveRef(DriveSamples drive, String name) {
		setName(name);
		setDrive(drive);
	}
	
	public DriveRef() {}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public DriveSamples getDrive() {
		return drive;
	}
	
	public void setDrive(DriveSamples drive) {
		this.drive = drive;
	}
	
	public boolean hasDrive() {
		return drive != null;
	}
}
