package com.milab.drivingfluency.model;

public enum FluencyEventType {
	NONE,
	MODERATE, // above 0.2 G
	EXCEED_THRESHOLD, // Above 0.3g
	EXTREME          // above 4.5 G

}
