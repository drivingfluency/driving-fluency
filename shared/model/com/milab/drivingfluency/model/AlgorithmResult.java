package com.milab.drivingfluency.model;

import java.util.LinkedList;
import java.util.List;

public class AlgorithmResult {
	private List<RawEvent> rawEvents;
	private List<AggregatedEvent> aggregatedEvents;
	private double score;
	
	public AlgorithmResult() {
		rawEvents = new LinkedList<RawEvent>();
		aggregatedEvents = new LinkedList<AggregatedEvent>();
	}
	
	public void addRawEvent(RawEvent event) {
		rawEvents.add(event);
	}
	public void addAggregatedEvent(AggregatedEvent event) {
		aggregatedEvents.add(event);
	}

	public List<RawEvent> getRawEvents() {
		return rawEvents;
	}

	public void setRawEvents(List<RawEvent> rawEvents) {
		this.rawEvents = rawEvents;
	}

	public List<AggregatedEvent> getAggregatedEvents() {
		return aggregatedEvents;
	}

	public void setAggregatedEvents(List<AggregatedEvent> aggregatedEvents) {
		this.aggregatedEvents = aggregatedEvents;
	}
	
	public double getScore() {
		return score;
	}
	
	public void setScore(double score) {
		this.score = score;
	}
}
