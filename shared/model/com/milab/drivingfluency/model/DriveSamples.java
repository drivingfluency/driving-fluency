package com.milab.drivingfluency.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import com.milab.drivingfluency.db.Persistance.SampleType;

/**
 * Represents a drive
 */
public class DriveSamples implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private List<Sample> accelerometerSamples;
	private List<Sample> magneticFieldSamples;
	
	public DriveSamples() {
		accelerometerSamples = new LinkedList<Sample>();
		magneticFieldSamples = new LinkedList<Sample>();
	}
	
	public void addAccelerometerSample(Sample sample) {
		accelerometerSamples.add(sample);
	}
	
	public void addMagneticFieldSample(Sample sample) {
		magneticFieldSamples.add(sample);
	}
	
	public void addSample(Sample sample, SampleType sampleType) {
		if (sampleType == SampleType.ACCELEROMETER) {
			accelerometerSamples.add(sample);
		}
		else {
			magneticFieldSamples.add(sample);
		}
	}
	
	public List<Sample> getAccelerometerSamples() {
		return accelerometerSamples;
	}
	
	public List<Sample> getMagneticFieldSamples() {
		return magneticFieldSamples;
	}
	
	public List<Sample> getSamples(SampleType sampleType) {
		if (sampleType == SampleType.ACCELEROMETER) {
			return accelerometerSamples;
		}
		else {
			return magneticFieldSamples;
		}
	}
	
	public void setAccelerometerSamples(List<Sample> samples) {
		accelerometerSamples = samples;
	}
	
	public void setMagneticFieldSamples(List<Sample> samples) {
		magneticFieldSamples = samples;
	}
	
	public void setSamples(List<Sample> samples, SampleType sampleType) {
		if (sampleType == SampleType.ACCELEROMETER) {
			setAccelerometerSamples(samples);
		}
		else {
			setMagneticFieldSamples(samples);
		}
	}
	
	public long getStartTime() {
		return accelerometerSamples.get(0).getTimestamp();
	}
	
	public long getEndTime() {
		return accelerometerSamples.get(accelerometerSamples.size() - 1).getTimestamp();
	}
	
	public float getDurationInSeconds() {
		return (getEndTime() - getStartTime()) / 1000;
	}
	
}
