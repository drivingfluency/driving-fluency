package com.milab.drivingfluency.model;

import java.util.Date;

public class RawEvent {
	public enum Intensity {
		NONE,				// above 0.0 G
		MODERATE, 			// above 0.2 G
		EXCEED_THRESHOLD, 	// above 0.3 G
		EXTREME 			// above 4.5 G
	}
	
	public long time;
	public long duration;
	public Intensity intensity;
	public double xRatio;
	public double yRatio;
	
	public RawEvent() {}
	
	public RawEvent(RawEvent other) {
		this.intensity = other.intensity;
		this.time = other.time;
		this.xRatio = other.xRatio;
		this.yRatio = other.yRatio;
		this.duration = other.duration;
	}
	
	public RawEvent(Intensity p, double xR, double yR) {
		this(p, xR, yR, new Date().getTime() / 1000000);
	}
	
	public RawEvent(Intensity p, double xR, double yR, long t) {
		this(p, xR, yR, t, 0);
	}
	
	public RawEvent(Intensity p, double xR, double yR, long t, long duration) {
		this.time = t;
		this.intensity = p;
		this.xRatio = xR;
		this.yRatio = yR;
		this.duration = duration;
	}
	
	public String toString() {
		return String.format("%s", intensity);
	}
	
}
