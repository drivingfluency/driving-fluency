package com.milab.drivingfluency.algorithm;

import java.util.Iterator;

import com.milab.drivingfluency.model.AggregatedEvent;
import com.milab.drivingfluency.model.AlgorithmResult;
import com.milab.drivingfluency.model.DriveSamples;
import com.milab.drivingfluency.model.RawEvent;
import com.milab.drivingfluency.model.Sample;
import com.milab.drivingfluency.model.RawEvent.Intensity;

/**
 * This is the main algorithm class.
 * There are two usages:
 * 		1. Pass samples live to this class to receive aggregated,classified events 
 * 		as they come.
 * 		2. Call the static resultForDrive method to run the algorithm offline
 */
public class FluencySensing 
	implements AggregatedEventListener, RawEventListener, SampleListener, MagneticFieldListener {
	
	private AlgorithmResult result;
	private Listener listener;
	private TurnDetector turnDetector;
	
	public static interface Listener extends AggregatedEventListener, MagneticFieldListener {}
	
	private Scorer scorer;
	
	/**
	 * SampleRawEventConverter receives samples and notifies us of raw events
	 */
	private SampleRawEventConverter sampleRawEventConverter;
	
	/**
	 * Aggregator receives raw events and aggregates them to AggregatedEvents
	 */
	private Aggregator aggregator;
	
	/**
	 * Classificator tells us what happened (right/left turn, acceleration/stop)
	 */
	private Classificator classificator;

	public FluencySensing(Listener listener) {
		this.listener = listener;
		
		result = new AlgorithmResult();
		sampleRawEventConverter = new SampleRawEventConverter(this);
		aggregator = new Aggregator(this);
		turnDetector = new TurnDetector(this);
		classificator = new Classificator();
		scorer = new Scorer();
	}
	
	public FluencySensing() {
		this(null);
	}
	
	public double getXRatio() {
		return sampleRawEventConverter.getXRatio();
	}
	
	public double getYRatio() {
		return sampleRawEventConverter.getYRatio();
	}
	
	/**
	 * Calculates a result for the drive
	 * @param drive
	 * @return
	 */
	public static AlgorithmResult resultForDrive(DriveSamples drive) {
		FluencySensing sensing = new FluencySensing();
		
		//get iterators to accelerometer and magnetic field samples
		Iterator<Sample> acceleromterIterator = drive.getAccelerometerSamples().iterator();
		Iterator<Sample> magneticIterator = drive.getMagneticFieldSamples().iterator();
		
		//get the first ones
		Sample currentAccelerometer = acceleromterIterator.next();
		Sample currentMagnetic = magneticIterator.next();
		
		//while there are samples left
		while (currentAccelerometer != null || currentMagnetic != null) {
			long accelerometerTimestamp;
			long magneticTimestamp;
			
			if (currentMagnetic == null) {
				accelerometerTimestamp = 0;
				magneticTimestamp = 1;
			}
			else if (currentAccelerometer == null) {
				magneticTimestamp = 0;
				accelerometerTimestamp = 1;
			}
			else {
				magneticTimestamp = currentMagnetic.getTimestamp();
				accelerometerTimestamp = currentAccelerometer.getTimestamp();
			}
			
			//if we ran out of accelerometer samples or the current magnetic is before the current accelerometer
			if (magneticTimestamp < accelerometerTimestamp) {
				sensing.onMagneticSample(currentMagnetic);
				if (magneticIterator.hasNext()) {
					currentMagnetic = magneticIterator.next();
				}
				else {
					currentMagnetic = null;
				}
			}
			else {
				sensing.onAccelerometerSample(currentAccelerometer);
				if (acceleromterIterator.hasNext()) {
					currentAccelerometer = acceleromterIterator.next();
				}
				else {
					currentAccelerometer = null;
				}
			}
		}
		sensing.stop(drive.getEndTime());
		
		return sensing.getResult();
	}
	
	@Override
	public void onMagneticSample(Sample sample) {
		classificator.onMagneticSample(sample);
		turnDetector.onMagneticSample(sample);
	}
	
	@Override
	public void onAccelerometerSample(Sample sample) {
		//if this is the first sample, record it in result
		if (result.getAggregatedEvents().isEmpty()) {
			result.addAggregatedEvent(new AggregatedEvent(
					Intensity.NONE, 
					Classification.DRIVE_START,
					0,
					0,
					sample.getTimestamp()));
		}
		
		//tell the aggregator that a new sample is available
		aggregator.onNewSample(sample.getTimestamp());
		
		//tell SampleRawEventConverter that a new sample is available
		sampleRawEventConverter.addSample(sample);
		
		//tell the scorer of the new sample
		scorer.onSample(sample);
	
		turnDetector.onAccelerometerSample(sample);
	}
	
	/**
	 * Call this when the drive is over
	 * @param time
	 * 		The current timestamp in milliseconds
	 */
	public void stop(long time) {
		//let aggregator know the drive is about the end
		aggregator.onDone(time);
		
		//mark end of drive
		result.addAggregatedEvent(new AggregatedEvent(
				Intensity.NONE, 
				Classification.DRIVE_END, 
				0, 
				0,
				time
		));
		result.setScore(scorer.calcTotalScore(time));
	}
	
	@Override
	public void onRawEvent(RawEvent event) throws Exception {
		result.addRawEvent(event);
		aggregator.onRawEvent(event);
		classificator.onRawEvent(event);
	}
	
	@Override
	public void onAggregatedEvent(AggregatedEvent event) {
		try {
			event.classification = classificator.winnerVerdict();
			result.addAggregatedEvent(event);
			
			scorer.onAggregatedEvent(event);
			
			if (event.intensity.ordinal() > Intensity.MODERATE.ordinal() &&
				listener != null) {
				try {
					listener.onAggregatedEvent(event);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public AlgorithmResult getResult() {
		return result;
	}

	@Override
	public void onMagneticFieldTurn(AggregatedEvent event) {
		if (listener != null) {
			listener.onMagneticFieldTurn(event);
		}
	}
	
}
