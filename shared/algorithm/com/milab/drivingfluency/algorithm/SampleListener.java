package com.milab.drivingfluency.algorithm;

import com.milab.drivingfluency.model.Sample;

public interface SampleListener {
	public void onAccelerometerSample(Sample sample);
	public void onMagneticSample(Sample sample);
}
