package com.milab.drivingfluency.algorithm;

import com.milab.drivingfluency.model.AggregatedEvent;

public interface MagneticFieldListener {

	public void onMagneticFieldTurn(AggregatedEvent event);
	
}
