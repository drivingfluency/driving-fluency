package com.milab.drivingfluency.algorithm;

import java.util.LinkedList;

import com.milab.drivingfluency.algorithm.Compass.AngleEvent;
import com.milab.drivingfluency.model.AggregatedEvent;
import com.milab.drivingfluency.model.RawEvent.Intensity;
import com.milab.drivingfluency.model.Sample;

public class TurnDetector implements SampleListener, Compass.Listener {
	
	//out of respect to the original writers of this code,
	//please keep this magenticEvents =)
	private LinkedList<AngleEvent> magenticEvents = new LinkedList<AngleEvent>();

	private static final double STEADY_DEGREE_DELTA_THRESHOLD = 8;
	private static final long TIME_STEADY_THRESHOLD = 2000;
	private static final long TIME_BETWEEN_EVENTS = 1000;
	private static final double MAX_DEGREE_DELTA_THRESHOLD_PER_SEC = 45;
	private static final double MIN_OVERALL_TURN_DEGREE_DELTA = 30;
	
	private Compass compass = new Compass(this);
	private AngleEvent lastEvent;
	private MagneticFieldListener listener;
	
	public TurnDetector(MagneticFieldListener listener) {
		this.listener = listener;
	}
	
	@Override
	public void onAccelerometerSample(Sample sample) {}

	@Override
	public void onMagneticSample(Sample sample) {
		compass.onMagneticSample(sample);
	}
	
	
	private double calcAngle(AngleEvent sample1, AngleEvent sample2) {
		double angle1 = sample1.angle + 180;
		double angle2 = sample2.angle + 180;
		// (300 degrees vs 10 degrees) is 70  degrees and not 290 degrees. This is what we verify here
		return Math.min(Math.abs(angle1 - angle2), 360 - Math.abs(angle1 - angle2));
		
	}
	
	private boolean isValidTurn(LinkedList<AngleEvent> samples) {
		if (samples.size() <= 1) {
			return false;
		}
		
		//test 1 - relative max degree change
		for (int i = 0; i < samples.size()-1; i++) {
			AngleEvent first = samples.get(i);
			AngleEvent second = samples.get(i+1);
			
			if (calcAngle(first, second) >= MAX_DEGREE_DELTA_THRESHOLD_PER_SEC) {
				System.out.println("** first " + first.angle);
				System.out.println("** second " + second.angle);
				System.out.println("** EXCEED MAX PER SEC " + calcAngle(first, second));
				return false;
			}
		}
		
		//test 2 - overall turn degree change 
		if ( calcAngle(samples.getFirst(), samples.getLast()) < MIN_OVERALL_TURN_DEGREE_DELTA) {
			System.out.println("** LESS THAN MIN OVERALL " + calcAngle(samples.getFirst(), samples.getLast()));
			return false;
		}
		
		/**
		//test 3 - does turn go in a single direction
		boolean isPositive = samples.get(1).getX() - samples.get(0).getX() > 0;
		for (int i = 1; i < samples.size()-1; i++) {
			boolean newIsPositive = samples.get(i+1).getX() - samples.get(i).getX() > 0;
			if (isPositive != newIsPositive) {
				return false;
			}
		}
		*/
		
		return true;
	}

	@Override
	public void onAngleChanged(AngleEvent event) {
		//System.out.println("***** " + (event.angle+180));
		
		//if this is the first sample, just save it
		if (lastEvent == null) {
			lastEvent = event;
		}
		else {
			//if a second has passed
			if (event.timestamp - lastEvent.timestamp >= TIME_BETWEEN_EVENTS) {
				System.out.println("NEW SAMPLE");
				
				//there is a significant change
				if (calcAngle(event, lastEvent) >= STEADY_DEGREE_DELTA_THRESHOLD) {
					magenticEvents.add(event);
					System.out.println("NOT STEADY " + calcAngle(event, lastEvent));
				}
				else {
					System.out.println("STEADY " + calcAngle(event, lastEvent));
					//if was not steady (but now steady)
					if (!magenticEvents.isEmpty()) {
						//if enough time has passed since last wild sample
						if (event.timestamp - magenticEvents.getLast().timestamp >= TIME_STEADY_THRESHOLD) {
							System.out.println("ENOUGHT STEADY");
							if (isValidTurn(magenticEvents)) try {
								System.out.println("** VALID TURN " + calcAngle(magenticEvents.getFirst(), event));
								listener.onMagneticFieldTurn(new AggregatedEvent(
									Intensity.EXCEED_THRESHOLD, 
									Classification.TURN_LEFT,
									0,
									0,
									magenticEvents.getFirst().timestamp));
							}
							catch (Exception e) {
								e.printStackTrace();
							}
							
							//reset memory
							magenticEvents.clear();
							lastEvent = null;
						}
					}
				}
				lastEvent = event;
			}
		}
	}

}
