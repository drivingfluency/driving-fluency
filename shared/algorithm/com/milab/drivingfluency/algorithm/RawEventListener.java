package com.milab.drivingfluency.algorithm;

import com.milab.drivingfluency.model.RawEvent;

public interface RawEventListener {
	public void onRawEvent(RawEvent event) throws Exception;
}
