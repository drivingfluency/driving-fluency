package com.milab.drivingfluency.algorithm;

public enum Classification {
	TURN_LEFT,		
	TURN_RIGHT,		
	ACCELERATE,		
	STOP,
	SPEED_BUMP,
	DRIVE_START,
	DRIVE_END
}

