package com.milab.drivingfluency.algorithm;

import com.milab.drivingfluency.model.Sample;

/**
 * Based on code
 */
public class Compass implements SampleListener {
	
	// data for orientation values filtering (average using a ring buffer)
	static private final int RING_BUFFER_SIZE=10;
	private float[][][] mAnglesRingBuffer;
	private int mNumAngles;
	private int mRingBufferIndex;
	private float[][] mAngles;
	
	public static class AngleEvent {
		double angle;
		long timestamp;
		
		public AngleEvent(double angle, long timestamp) {
			this.angle = angle;
			this.timestamp = timestamp;
		}
		public AngleEvent() {
			//kiwi|
			//    V
			this(0,0);
		}
	}
	
	public static interface Listener {
		public void onAngleChanged(AngleEvent event);
	}
	private Listener listener;

	public Compass(Listener listener) {
		// initialize the ring buffer for orientation values
        mNumAngles=0;
        mRingBufferIndex=0;
        mAnglesRingBuffer=new float[RING_BUFFER_SIZE][3][2];
        mAngles=new float[3][2];
        mAngles[0][0]=0;
        mAngles[0][1]=0;
        mAngles[1][0]=0;
        mAngles[1][1]=0;
        mAngles[2][0]=0;
        mAngles[2][1]=0;
		
		this.listener = listener;
	}
	
	@Override
	public synchronized void onAccelerometerSample(Sample sample) {}
	
	@Override
	public synchronized void onMagneticSample(Sample sample) {
		if(mNumAngles==RING_BUFFER_SIZE) {
			// subtract oldest vector
    		mAngles[0][0]-=mAnglesRingBuffer[mRingBufferIndex][0][0];
    		mAngles[0][1]-=mAnglesRingBuffer[mRingBufferIndex][0][1];
    		mAngles[1][0]-=mAnglesRingBuffer[mRingBufferIndex][1][0];
    		mAngles[1][1]-=mAnglesRingBuffer[mRingBufferIndex][1][1];
    		mAngles[2][0]-=mAnglesRingBuffer[mRingBufferIndex][2][0];
    		mAngles[2][1]-=mAnglesRingBuffer[mRingBufferIndex][2][1];
		} else {
			mNumAngles++;
		}

		// convert angles into x/y
		mAnglesRingBuffer[mRingBufferIndex][0][0]=(float) Math.cos(Math.toRadians(sample.getX()));
		mAnglesRingBuffer[mRingBufferIndex][0][1]=(float) Math.sin(Math.toRadians(sample.getX()));
		mAnglesRingBuffer[mRingBufferIndex][1][0]=(float) Math.cos(Math.toRadians(sample.getY()));
		mAnglesRingBuffer[mRingBufferIndex][1][1]=(float) Math.sin(Math.toRadians(sample.getY()));
		mAnglesRingBuffer[mRingBufferIndex][2][0]=(float) Math.cos(Math.toRadians(sample.getZ()));
		mAnglesRingBuffer[mRingBufferIndex][2][1]=(float) Math.sin(Math.toRadians(sample.getZ()));
		
		// accumulate new x/y vector
		mAngles[0][0]+=mAnglesRingBuffer[mRingBufferIndex][0][0];
		mAngles[0][1]+=mAnglesRingBuffer[mRingBufferIndex][0][1];
		mAngles[1][0]+=mAnglesRingBuffer[mRingBufferIndex][1][0];
		mAngles[1][1]+=mAnglesRingBuffer[mRingBufferIndex][1][1];
		mAngles[2][0]+=mAnglesRingBuffer[mRingBufferIndex][2][0];
		mAngles[2][1]+=mAnglesRingBuffer[mRingBufferIndex][2][1];
		
		mRingBufferIndex++;
		if(mRingBufferIndex==RING_BUFFER_SIZE) {
			mRingBufferIndex=0;
		}
		
		// convert back x/y into angles
		float azimuth=(float) Math.toDegrees(Math.atan2((double)mAngles[0][1], (double)mAngles[0][0]));
		float pitch=(float) Math.toDegrees(Math.atan2((double)mAngles[1][1], (double)mAngles[1][0]));
		float roll=(float) Math.toDegrees(Math.atan2((double)mAngles[2][1], (double)mAngles[2][0]));
		
		if (listener != null) {
			listener.onAngleChanged(new AngleEvent(azimuth, sample.getTimestamp()));
		}
	}
}
