package com.milab.drivingfluency.algorithm;

import com.milab.drivingfluency.model.RawEvent;
import com.milab.drivingfluency.model.FluencyEventType;
import com.milab.drivingfluency.model.Sample;

public class SampleRawEventConverter {
	
	private RawEventListener listener;
	private FluencyEventType eventState;
	
	private double _initX;
	private double _initY;
	private double _initZ;

	private double _Xbase;
	private double _Ybase;
	private double _Zbase;

	private double _yzMagnitude;
	private double _y_yzProportion;
	private double _z_yzProportion;

	private double _initMagnitude; 
	private double _initYZMagnitude;
	private double _Magnitude; 

	private double _AccumX;
	private double _AccumY;
	private double _AccumZ;

	private double _initOrientationX;
	private double _initOrientationY;
	private double _initOrientationZ;

	private static final int INIT_INITIAL_VAL = -7;
	private int _initCounter = INIT_INITIAL_VAL;
	
	private static final double MAGNITUDE_EXTREME = 1.9;
	private static final double MAGNITUDE_EXCEED = 1.45;
	private static final double MAGNITUDE_MODERATE = 1.3;

	private int _initOrientationCounter = INIT_INITIAL_VAL;

	public SampleRawEventConverter(RawEventListener listener) {
		_initX  = -1;
		_initY  = -1;
		_initZ  = -1;	
		_AccumX = 0;
		_AccumY = 0;
		_AccumZ = 0;
		
		this.listener = listener;
		
		eventState = FluencyEventType.NONE;

		_initOrientationCounter = INIT_INITIAL_VAL;
		_initCounter = INIT_INITIAL_VAL;
	}
	
	public SampleRawEventConverter() {
		this(null);
	}
	
	public double getXRatio() {
		double ratio = _AccumX - _initX;

		int sign = (ratio < 0) ? -1 : 1;
		if (Math.abs(ratio) < 0.3) {
			ratio = 0;
		}
		ratio = sign * (ratio * ratio) / (_initMagnitude);

		return ratio;
	}

	private void init(double x,double y, double z) {
		if(_initCounter == INIT_INITIAL_VAL) {
			//_initCounter++;
			_initX = x;
			_initY = y;
			_initZ = z;
			_AccumX = x;
			_AccumY = y;
			_AccumZ = z;	
			_initMagnitude = x*x+y*y+z*z;

			_yzMagnitude = y*y+z*z;
			_initYZMagnitude = _yzMagnitude;

			_y_yzProportion = (y*y)/_yzMagnitude;
			_z_yzProportion = (z*z)/_yzMagnitude;

			_Xbase = _initX/_initMagnitude;
			_Ybase = _initY/_initMagnitude;
			_Zbase = _initZ/_initMagnitude;
		}
		else { // Init and smooth init values
			_initX = x;
			_initY = y;
			_initZ = z;
		}
	}

	private void initOrientation(double x, double y, double z) {
		_initOrientationX = x;
		_initOrientationY = y;
		_initOrientationZ = z;
	}
	
	public void addOrientationSample(double x,double y, double z) {
		if(INIT_INITIAL_VAL == _initOrientationCounter){
			initOrientation(x,y,z);
		} 
		else if (_initOrientationCounter < 1) {
			_initOrientationX= 0.73*_initOrientationX+0.27*x;
			_initOrientationY= 0.73*_initOrientationY+0.27*x;
			_initOrientationZ= 0.73*_initOrientationZ+0.27*z; 			
		}

		_initOrientationCounter++;
	}
	
	public void addSample(Sample sample) {
		addSample(sample.getX(), sample.getY(), sample.getZ(), sample.getTimestamp());
	}
	
	/**
	 * Use this overload for off line usage.
	 * @param x
	 * @param y
	 * @param z
	 * @param timestamp
	 * 		The time in which the sample was registered
	 */
	public void addSample(double x,double y, double z, long timestamp) {
		if(_initCounter == INIT_INITIAL_VAL) {
			init(x,y,z);
		}
		else {
			_AccumX= 0.73*_AccumX+0.27*x;
			_AccumY= 0.73*_AccumY+0.27*y;
			_AccumZ= 0.73*_AccumZ+0.27*z;
			_yzMagnitude = _AccumY*_AccumY+_AccumZ*_AccumZ;
			_Magnitude = _AccumX*_AccumX+_AccumY*_AccumY+_AccumZ*_AccumZ;
		}

		if (_initCounter < 1) { // Smooth the initial value through several samples
			init(_AccumX,_AccumY,_AccumZ);
		}
		_initCounter++;
		if (_initCounter > 10) {			
			//calculate new state
			FluencyEventType newState = FluencyEventType.NONE;
			if (_Magnitude > MAGNITUDE_EXTREME *_initMagnitude) {
				newState = FluencyEventType.EXTREME;
			} 
			else if (_Magnitude > MAGNITUDE_EXCEED *_initMagnitude ) {
				newState = FluencyEventType.EXCEED_THRESHOLD;
			} 
			else if (_Magnitude > MAGNITUDE_MODERATE *_initMagnitude) {
				newState = FluencyEventType.MODERATE;
			}
			
			//update state
			if (eventState != newState) {
				eventState = newState;
				//create a FluencyEvent object
				RawEvent newEvent = new RawEvent(
						RawEvent.Intensity.valueOf(eventState.name()),
						getXRatio(), getYRatio(), timestamp
				);
				
				try {
					listener.onRawEvent(newEvent);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (_initCounter == 100) {
				_initCounter = 11;
			}
		}
	}
	
	public double getYRatio() {
		double deltaY = _AccumY - _initY;
		double deltaZ = _AccumZ - _initZ;

		double yMagnitude = (Math.pow(deltaY, 2) + Math.pow(deltaZ, 2)) /
							(Math.pow(_initX, 2) + Math.pow(_initY, 2) + Math.pow(_initZ, 2));
		//yMagnitude = yMagnitude*9.3;

		int sign = 1;
		/*
		// Define the sign
		if ((Math.abs(_AccumY - _initY)<1) && (Math.abs(_AccumZ - _initZ)<1)) {
			sign = 0;
		}
		*/

		if ((_AccumY < _initY+0.5) || (_AccumZ > _initZ-0.5)) {
			sign = -1;
		}
		
		return yMagnitude * sign;
	}

	public double getYZRatio() {
		double deltaY = 0;
		double deltaZ = 0;
		if(Math.abs(_initY) <0.01) {
			deltaY = 0.01;
		}		
		if(Math.abs(_initZ) <0.01) {
			deltaZ = 0.01;
		}		

		double ratio =	(_z_yzProportion * ((_AccumY + deltaY) / (_initY + deltaY)) +
				_y_yzProportion * ((_AccumZ + deltaZ) / (_initZ + deltaZ)));
		if(_initCounter < 10)
			ratio = 1;
		
		return 0;
	}
}
