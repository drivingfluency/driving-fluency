package com.milab.drivingfluency.algorithm;

import java.util.LinkedList;
import java.util.List;

import com.milab.drivingfluency.model.RawEvent;
import com.milab.drivingfluency.model.Sample;

public class Classificator 
	implements RawEventListener, SampleListener {

	private double accumX = 0;
	private double accumY = 0;
	
	private List<RawEvent> events;
	
	public Classificator() {
		resetAccumulators();
	}
	
	public void onRawEvent(RawEvent event) {
		/** First algorithm for magnetic field
		//in start of event
		if (events.isEmpty()) {
			startMagneticSample = lastMagneticSample;
		}
		*/
		
		accumX += event.xRatio;
		accumY += event.yRatio;
		events.add(event);
	}

	private void resetAccumulators () {
		accumX = 0;
		accumY = 0;
		events = new LinkedList<RawEvent>();
	}

	public Classification winnerVerdict() {
//		if (events.get(0).xRatio >= events.get(0).yRatio) {
		if (Math.abs(accumX) >= Math.abs(accumY)) {
			if (accumX >= 0) {
				resetAccumulators();
				return Classification.TURN_RIGHT;
			} else {
				resetAccumulators();
				return Classification.TURN_LEFT;
			}
		}
		else {
			if (accumY >= 0) {
				resetAccumulators();
				return Classification.ACCELERATE;
			}
			else {
				Classification res;
				if (isBump()) {	
					res = Classification.SPEED_BUMP;
				}
				else {
					res = Classification.STOP;
				}
				resetAccumulators();
				return res;
			}
		}
	}
	
	@Override
	public void onMagneticSample(Sample sample) {}
	
	@Override
	public void onAccelerometerSample(Sample sample) {}
	
	private boolean isBump() {
		List<Double> downSlopes = new LinkedList<Double>();
		List<Double> upSlopes = new LinkedList<Double>();
		
		RawEvent slopeStartEvent = null;
		Boolean isUpSlope = null;
		for (int i = 0; i < events.size() - 1; i++) {
			RawEvent current = events.get(i);
			RawEvent next = events.get(i+1);
			
			// this is a start
			if (isUpSlope == null) {
				isUpSlope = next.yRatio - current.yRatio > 0;
				slopeStartEvent = current;
			}
			// in middle of slope
			else {
				boolean isUp = next.yRatio - current.yRatio > 0;
				
				// slope change
				if (isUp != isUpSlope) {
					double slope = calcSlope(current, slopeStartEvent);
					if (slope > 0) {
						upSlopes.add(slope);
					}
					else {
						downSlopes.add(slope);
					}
					
					slopeStartEvent = current;
					isUpSlope = isUp;
				}
			}
		}
		
		// stopped not on last
		RawEvent lastEvent = events.get(events.size() - 1);
		if (slopeStartEvent != lastEvent) {
			double slope = calcSlope(lastEvent, slopeStartEvent);
			if (slope > 0) {
				upSlopes.add(slope);
			}
			else {
				downSlopes.add(slope);
			}
		}
				
		return upSlopes.size() + downSlopes.size() >= 3;// && downSlopes.size() >= 2;
	}
	
	private double calcSlope(RawEvent e1, RawEvent e2) {
		return (e1.yRatio - e2.yRatio) /
			   (e1.time - e2.time);
	}
}
