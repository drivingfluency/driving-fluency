//package com.milab.drivingfluency.algorithm;
//import android.app.Activity;
//import android.hardware.Sensor;
//import android.hardware.SensorEvent;
//import android.hardware.SensorEventListener;
//import android.hardware.SensorManager;
//import android.os.Bundle;
//import android.widget.TextView;
//import android.widget.Button;
//import android.view.View;
//
//public class angle extends Activity{
//	
//	/** Called when the activity is first created. */
//    @Override
//    public void onCreate(Bundle savedInstanceState)
//    {
//        super.onCreate(savedInstanceState);
//        //setContentView(R.layout.main);        
//
//        SensorManager sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);        
//
//        final float[] mValuesMagnet      = new float[3];
//        final float[] mValuesAccel       = new float[3];
//        final float[] mValuesOrientation = new float[3];
//        final float[] mRotationMatrix    = new float[9];
//
//        //final Button btn_valider = (Button) findViewById(R.id.btn1);
//        //final TextView txt1 = (TextView) findViewById(R.id.textView1);
//        final SensorEventListener mEventListener = new SensorEventListener() {
//            public void onAccuracyChanged(Sensor sensor, int accuracy) {
//            }
//
//            public void onSensorChanged(SensorEvent event) {
//                // Handle the events for which we registered
//                switch (event.sensor.getType()) {
//                    case Sensor.TYPE_ACCELEROMETER:
//                        System.arraycopy(event.values, 0, mValuesAccel, 0, 3);
//                        break;
//
//                    case Sensor.TYPE_MAGNETIC_FIELD:
//                        System.arraycopy(event.values, 0, mValuesMagnet, 0, 3);
//                        break;
//                }
//            };
//        };
//
//        // Set the event lisetner up and register it with the sensor manager along with the wanted sensor.
//        setListners(sensorManager, mEventListener);
//
//        /**
//        btn_valider.setOnClickListener(new View.OnClickListener()
//        {
//            public void onClick(View view)
//            {
//                SensorManager.getRotationMatrix(mRotationMatrix, null, mValuesAccel, mValuesMagnet);
//                SensorManager.getOrientation(mRotationMatrix, mValuesOrientation);
//                final CharSequence test;
//                test = "results: " + mValuesOrientation[0] +" "+mValuesOrientation[1]+ " "+ mValuesOrientation[2];
//                txt1.setText(test);
//            }
//        });
//        */
//
//    }
//
//    // Register the event listener and sensor type.
//    public void setListners(SensorManager sensorManager, SensorEventListener mEventListener)
//    {
//        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), 
//                SensorManager.SENSOR_DELAY_NORMAL);
//        sensorManager.registerListener(mEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), 
//                SensorManager.SENSOR_DELAY_NORMAL);
//    }
//
//}
