package com.milab.drivingfluency.algorithm;

import com.milab.drivingfluency.model.AggregatedEvent;

public interface AggregatedEventListener {
	public void onAggregatedEvent(AggregatedEvent event) throws Exception;
}
