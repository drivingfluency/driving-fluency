package com.milab.drivingfluency.algorithm;

import java.util.ArrayList;
import java.util.List;

import com.milab.drivingfluency.model.AggregatedEvent;
import com.milab.drivingfluency.model.Sample;

public class Scorer {
	
	private int exceedThresholdEvents = 0;
	private int extremeEvents = 0;

	private List<Double> scores = new ArrayList<Double>();
		
	private long intervalStartTime;
	private boolean isFirstSample = true;
	private long lastSampleTime;
	private long normalValue;
	
	private static final double EXTERME_POINTS = 4;
	private static final double EXCEED_THRESHOLD_POINTS = 2;

	// 10 minutes threshold (in milliseconds) for each score
	public final static int INTERVAL_DURATION_MILLI = 600000;
	
	public final static int ONE_MINUTE_IN_MILI = 60000;
	public final static int TWO_MINUTES_IN_MIILI = 120000;
	public final static int TEN_SECONDS_IN_MILI = 10000;
	

	public void onAggregatedEvent(AggregatedEvent event) {
		//ignore bump events
		if (event.classification == Classification.SPEED_BUMP) {
			return;
		}
		
		switch (event.intensity) {
		case EXCEED_THRESHOLD:
			exceedThresholdEvents++;
			break;

		case EXTREME:
			extremeEvents++;
			break;

		default:
			break;
		}
	}
	
	public void onSample(Sample sample) {
		if (isFirstSample) {
			intervalStartTime = sample.getTimestamp();
			isFirstSample = false;
		}
		else {
			if ((sample.getTimestamp() - intervalStartTime) > INTERVAL_DURATION_MILLI) {
				scores.add(calcIntervalScore());
				exceedThresholdEvents = 0;
				extremeEvents = 0;
				intervalStartTime = sample.getTimestamp();
				
			}
		}
		// For normal calculation
		lastSampleTime = sample.getTimestamp();
	}

	/**
	 * Returns the score for the current interval time
	 * Normal Example:
	 * If the driving duration was 37 minutes 
	 * There are 3 scores for ten minutes - and another score for the 7 minutes
	 * Which is just the same - but normalized to 7
	 * Score = 100 - 10/7 *C(bad points)
	 * @return
	 */
	private double calcIntervalScore() {
		double badPoints = EXTERME_POINTS * extremeEvents + EXCEED_THRESHOLD_POINTS * exceedThresholdEvents;
		double totalIntervalScore;
		
		// Is normalize necessary for current Interval Score  ?
		if ( (normalValue = lastSampleTime - intervalStartTime) < INTERVAL_DURATION_MILLI &&
			
			//for optimization we normalize only events which are longer than 1 minute
			(normalValue > ONE_MINUTE_IN_MILI) ) {
			
				totalIntervalScore = (double) 100 - (INTERVAL_DURATION_MILLI / ONE_MINUTE_IN_MILI) / ((int)(normalValue / ONE_MINUTE_IN_MILI)) * badPoints;
				return verifyScore(totalIntervalScore);

		// no normal optimization
		}else { 
			totalIntervalScore = 100 - badPoints;
			return verifyScore(totalIntervalScore);
		}
	}
	
	public double verifyScore(double points) {
		return points < 0 ? 0 : points;
	}

	/**
	 * Calculates the TOTAL score of ALL the intervals plus the last normalized score (if needed)
	 * 
	 * @return
	 */
	public double calcTotalScore(long time) {
		double accumScore = 0.0;	
		for (Double score : scores) {
			accumScore += score;
		}
		
		accumScore += calcIntervalScore();
		return accumScore / (scores.size() + 1);
	}
}