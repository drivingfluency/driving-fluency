package com.milab.drivingfluency.algorithm;

import com.milab.drivingfluency.model.AggregatedEvent;
import com.milab.drivingfluency.model.RawEvent;
import com.milab.drivingfluency.model.RawEvent.Intensity;

public class Aggregator 
	implements RawEventListener {
	
	private AggregatedEventListener listener;
	private static final boolean DEBUG = false;
	
	public Aggregator(AggregatedEventListener listener) {
		this.listener = listener;
		
	}
	
	private enum State {
		START,
		IN_EVENT,
		WAITING_TO_END_EVENT,
	}
	
	private State state = State.START;
	private RawEvent maxEvent = null;
	private long startTime;
	private long timeToEnd;
	
	private static final int MILLISECONDS_TO_WAIT_BEFORE_END_EVENT = 1500;
	private static final int MILLISECONDS_MAX_EVENT_TIME = 5000;
	private static final double ACCELERATE_MULTIPLIER = 3;
	private static final double ACCELERATE_THRESHOLD = 0.03;
	
	public void onRawEvent(RawEvent event) {
		//boost accelerate events
		if (event.yRatio * ACCELERATE_MULTIPLIER > Math.abs(event.xRatio) &&
			event.yRatio > ACCELERATE_THRESHOLD) {
			event.intensity = Intensity.EXCEED_THRESHOLD;
			event.yRatio *= ACCELERATE_MULTIPLIER;
		}
		
		//NONE is what we refer to as "nothing happened"
		final RawEvent.Intensity NONE = RawEvent.Intensity.MODERATE;
		
		switch (state) {
		case START:
			//if something happened
			if (event.intensity.ordinal() > NONE.ordinal()) {
				switchToState(State.IN_EVENT);
				startTime = event.time;
				maxEvent = event;
			}
			break;
		case IN_EVENT:
			//if lower than moderate
			if (event.intensity.ordinal() <= NONE.ordinal()) {
				switchToState(State.WAITING_TO_END_EVENT);
				
				//store the time in which we entered WAITING_TO_END_EVENT
				timeToEnd = event.time;
			}
			//if higher than moderate
			else {
				//if new event is more powerful than max event
				if (maxEvent.intensity.ordinal() < event.intensity.ordinal()) {
					maxEvent = event;
				}
			}
			break;
		case WAITING_TO_END_EVENT:
			//if event is more than moderate
			if (event.intensity.ordinal() > NONE.ordinal()) {
				switchToState(State.IN_EVENT);
				
				//if new event is more powerful than max event
				if (maxEvent.intensity.ordinal() < event.intensity.ordinal()) {
					maxEvent = event;
				}
			}
			break;
		}
	}
	
	public void onNewSample(long time) {
		if (state == State.WAITING_TO_END_EVENT) {
			if (time - timeToEnd > MILLISECONDS_TO_WAIT_BEFORE_END_EVENT) {	
				endEvent(time);
			}
		}
		else if (state == State.IN_EVENT) {
			if (time - startTime > MILLISECONDS_MAX_EVENT_TIME) {
				endEvent(time);
			}
		}
	}
	
	public void onDone(long time) {
		if (state == State.IN_EVENT || state == State.WAITING_TO_END_EVENT) {
			endEvent(time);
		}
	}
	
	private void endEvent(long time) {
		switchToState(State.START);
		
		if (DEBUG)
			System.out.println("adding event: " + maxEvent.intensity);
		
		try {
			maxEvent.time = startTime;
			maxEvent.duration = time - maxEvent.time;
			listener.onAggregatedEvent(new AggregatedEvent(maxEvent));
			//listener.receiveEvent(new RawEvent(Intensity.NONE, 0, 0, time));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		maxEvent = null;
	}
	
	private void switchToState(State newState) {
		if (DEBUG) {
			System.out.println(String.format("%s->%s", state, newState));
		}
		state = newState;
	}
}
