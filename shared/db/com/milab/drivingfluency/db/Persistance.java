package com.milab.drivingfluency.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.milab.drivingfluency.common.TableFormatter;
import com.milab.drivingfluency.model.AggregatedEvent;
import com.milab.drivingfluency.model.AlgorithmResult;
import com.milab.drivingfluency.model.DriveRef;
import com.milab.drivingfluency.model.DriveSamples;
import com.milab.drivingfluency.model.RawEvent;
import com.milab.drivingfluency.model.Sample;

public class Persistance {

	private static Persistance instance;
	private File dbDirectory;

	public static void init(File dbDirectory) throws Exception {
		new Persistance(dbDirectory);
	}
	
	private Persistance(File dbDirectory) throws Exception {
		if (instance != null) {
			return;
		}
		
		this.dbDirectory = dbDirectory;

		if (!dbDirectory.exists()) {
			dbDirectory.mkdir();
		}
		
		instance = this;
	}

	public static Persistance getInstance() {
		return instance;
	}
	
	public boolean isDriveNameValid(String driveName) {
		try {
			new File(driveName).getCanonicalFile();
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Creates a new temporary drive.
	 * You can refer to this drive by calling getTemporaryDrive.
	 * Later on when the drive is finished, you can use nameTemporaryDrive to name it.
	 * @return
	 * 		A drive reference that points to the temporary drive.
	 */
	public DriveRef makeTemporaryDrive() {
		File driveDirectory = new File(dbDirectory, getTemporaryDrive().getName());
		if (driveDirectory.exists()) {
			deleteRecursive(driveDirectory);
		}
		driveDirectory.mkdir();
		
		return getTemporaryDrive();
	}
	
	/**
<<<<<<< HEAD
	 * Get the current temporary drive
=======
	 * Gets a reference to the current temporary drive
>>>>>>> Now records orientation data and uses it to classify turns.
	 * @return
	 */
	public DriveRef getTemporaryDrive() {
		return new DriveRef("temp");
	}
	
	/**
	 * Names the current temporary drive
	 * @param newName
	 * @return
	 */
	public DriveRef nameTemporaryDrive(String newName) {
		// rename drive directory
		File driveDirectory = new File(dbDirectory, getTemporaryDrive().getName());
		driveDirectory.renameTo(new File(dbDirectory, newName));
		
		return new DriveRef(newName);
	}
	
	/**
	 * Makes a drive name composed of a specific time
	 * @param date
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static String generateDateName(Date date) {
		return String.format(
				Locale.US,
				"%d-%02d-%02d %02d.%02d.%02d",
				date.getYear() + 1900,
				date.getMonth() + 1,
				date.getDate(),
				date.getHours(),
				date.getMinutes(),
				date.getSeconds());
	}
	
	/**
	 * Makes a drive name composed of the current time
	 * @return
	 */
	public static String generateDateName() {
		return generateDateName(new Date());
	}
	
	/**
	 * Gets a samples file of a specific file
	 * @param driveRef
	 * @param sampleType
	 * @return
	 */
	public File getSamplesFile(DriveRef driveRef, SampleType sampleType) {
		return new File(new File(dbDirectory, driveRef.getName()), sampleType.seriFileName());
	}
	
	/**
	 * Saves both CSV and raw samples
	 * @param driveRef
	 * @param drive
	 * @return
	 * @throws Exception
	 */
	public DriveRef saveSamples(final DriveRef driveRef, final List<Sample> drive, final SampleType sampleType) throws Exception {
		Thread saveSeriThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					saveSamplesSeri(driveRef, drive, sampleType);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		saveSeriThread.start();
		
		//save csv
		try {
			saveSamplesCSV(driveRef, drive, sampleType);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		saveSeriThread.join();
	
		return driveRef;
	}
	
	/**
	 * Saves samples in serialized format
	 * @param driveRef
	 * @param samples
	 * @param sampleType
	 * @throws Exception
	 */
	public void saveSamplesSeri(DriveRef driveRef, List<Sample> samples, SampleType sampleType) throws Exception {
		File driveDirectory = makeDriveDirIfNotExist(driveRef);
		File file = new File(driveDirectory, sampleType.seriFileName());
		
		ObjectOutputStream os = null;
		try {
			os = new ObjectOutputStream(new FileOutputStream(file));
			os.writeObject(samples);
		}
		finally {
			os.close();
		}
	}
	
	/**
	 * Saves samples in CSV format
	 * @param driveRef
	 * @param drive
	 * @param sampleType
	 * @throws Exception
	 */
	public void saveSamplesCSV(DriveRef driveRef, List<Sample> samples, SampleType sampleType) throws Exception {
		File driveDirectory = makeDriveDirIfNotExist(driveRef);
		File file = new File(driveDirectory, sampleType.csvFileName());
		
		PrintStream driveStream = null;
		try {
			driveStream = new PrintStream(file);
			
			String format = "%s,%s,%s,%s";
			
			driveStream.println(String.format(format, "x", "y", "z", "time"));
			
			for (Sample sample : samples) {
				driveStream.println(
						String.format(
								format, 
								sample.getX(),
								sample.getY(),
								sample.getZ(),
								sample.getTimestamp()
						)
				);
			}
		}
		finally {
			driveStream.close();
		}
	}
	
	/**
	 * Checks if a drive exists
	 * @param name
	 * @return
	 */
	public boolean doesDriveExist(String name) {
		return doesDriveExist(new DriveRef(name));
	}
	
	/**
	 * Checks if a drive exists
	 * @param driveRef
	 * @return
	 */
	public boolean doesDriveExist(DriveRef driveRef) {
		return new File(dbDirectory, driveRef.getName()).exists();
	}
	
	/**
	 * A context used for saving samples one by one asynchronously
	 */
	public class SamplesSaveContext {
		private int i;
		private Sample[] samples;
		private ConcurrentLinkedQueue<Sample[]> buffersToSave;
		private PrintStream outStream;
		private Thread savingThread;
		private Object cv;
		private volatile boolean shouldFlushBuffer;
		private volatile boolean isDone;
		private List<Sample> driveSamples;
		private DriveRef driveRef;
	}
	
	private static final int SAMPLES_BUFFER_SIZE = 150;
	
	/**
	 * Use this method to start saving drive samples.
	 * This method returns a context which you should keep to
	 * save more samples later.
	 * @param driveRef
	 * @return
	 * @throws Exception
	 */
	public SamplesSaveContext startSavingSamples(DriveRef driveRef, SampleType sampleType) throws Exception {
		final SamplesSaveContext context = new SamplesSaveContext();
		
		context.samples = new Sample[SAMPLES_BUFFER_SIZE];
		context.i = 0;
		context.driveSamples = new LinkedList<Sample>();
		context.buffersToSave = new ConcurrentLinkedQueue<Sample[]>();
		context.cv = new Object();
		context.shouldFlushBuffer = false;
		context.isDone = false;
		context.driveRef = driveRef;
		
		{
			File driveDirectory = makeDriveDirIfNotExist(driveRef);
			context.outStream = new PrintStream(new File(driveDirectory, sampleType.csvFileName()));
		}
		
		context.savingThread = new Thread(new Runnable() {
			@Override
			public void run() {
				String format = "%s,%s,%s,%s";
				try {
					while (!context.isDone) {
						try {
							// wait until we should save buffer
							while (!context.shouldFlushBuffer) {
								synchronized (context.cv) {
									context.cv.wait();
								}
							}
							context.shouldFlushBuffer = false;
							
							System.out.println("Flushing buffer");
							
							while (!context.buffersToSave.isEmpty()) {
								Sample[] buffer = context.buffersToSave.remove();
								for (Sample sample : buffer) {
									if (sample == null) {
										break;
									}
									context.outStream.println(
											String.format(
													format, 
													sample.getX(),
													sample.getY(),
													sample.getZ(),
													sample.getTimestamp()
											)
									);
								}
							}
						}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				finally {
					context.outStream.flush();
					context.outStream.close();
				}
			}
		});
		context.savingThread.start();
		
		return context;
	}
	
	/**
	 * Save a single sample within a samples saving context
	 * @param context
	 * @param sample
	 */
	public void saveSample(SamplesSaveContext context, Sample sample, SampleType sampleType) {
		context.driveSamples.add(sample);
		if (context.i >= context.samples.length) {
			context.buffersToSave.add(context.samples);
			context.shouldFlushBuffer = true;
			
			synchronized (context.cv) {
				context.cv.notify();
			}
			
			context.samples = new Sample[SAMPLES_BUFFER_SIZE];
			context.i = 0;
		}
		context.samples[context.i] = sample;
		context.i++;
	}
	
	public enum SampleType {
		ACCELEROMETER,
		MAGNETIC_FIELD;
		
		private String csvFileName() {
			switch (this) {
			case ACCELEROMETER:
				return "samples.csv";
			case MAGNETIC_FIELD:
				return "magnetic-field.csv";
				
			default:
				return "bla";
			}
		}
		
		private String seriFileName() {
			switch (this) {
			case ACCELEROMETER:
				return "samples.raw";
			case MAGNETIC_FIELD:
				return "magnetic-field.raw";
				
			default:
				return "bla";
			}
		}
	}
	
	public Thread endSavingSamples(final SamplesSaveContext context, final SampleType sampleType) throws Exception {
		context.isDone = true;
		context.buffersToSave.add(context.samples);
		context.shouldFlushBuffer = true;
		
		synchronized (context.cv) {
			context.cv.notify();
		}
		
		//save serialized samples asynchronously
		Thread savingSeriThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					saveSamplesSeri(context.driveRef, context.driveSamples, sampleType);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		savingSeriThread.start();
		
		return savingSeriThread;
	}
	
	public void endSavingSamplesBlocking(SamplesSaveContext context, SampleType sampleType) throws Exception {
		Thread savingSeriThread = endSavingSamples(context, sampleType);
		savingSeriThread.join();
		context.savingThread.join();
	}
	
	/**
	 * Saves the result of an algorithm
	 * @param driveRef
	 * @param result
	 * @throws Exception
	 */
	public void saveAlgorithmResult(final DriveRef driveRef, final AlgorithmResult result) throws Exception {
		Thread saveRawEventsThread = new Thread(new Runnable() {
			public void run() {
				try {
					saveRawEvents(driveRef, result.getRawEvents());
				}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		});
		saveRawEventsThread.start();
		
		saveAggregatedEvents(driveRef, result.getAggregatedEvents());
		saveRawEventsThread.join();
	}
	
	public void saveAggregatedEvents(DriveRef driveRef, List<AggregatedEvent> events) throws FileNotFoundException {
		File driveDirectory = makeDriveDirIfNotExist(driveRef);
		saveAggregatedEventsCSV(new File(driveDirectory, "aggregated-events.csv"), events);
	}
	
	public void saveRawEvents(DriveRef driveRef, List<RawEvent> events) throws FileNotFoundException {
		File driveDirectory = makeDriveDirIfNotExist(driveRef);
		saveRawEventsCSV(new File(driveDirectory, "raw-events.csv"), events);
	}
	
	public void saveUserAggregatedEvents(DriveRef driveRef, List<AggregatedEvent> events) throws FileNotFoundException {
		File driveDirectory = makeDriveDirIfNotExist(driveRef);
		saveAggregatedEventsCSV(new File(driveDirectory, "user-aggregated-events.csv"), events);
	}
	
	private File makeDriveDirIfNotExist(DriveRef driveRef) {
		File driveDirectory = new File(dbDirectory, driveRef.getName());
		if (!driveDirectory.exists()) {
			driveDirectory.mkdir();
		}
		return driveDirectory;
	}
	
	public void saveDriveSummary(DriveRef driveRef, AlgorithmResult driveResults, long sessionDurationMilli, int score, int userScore) throws Exception {
		long sumExceedAggEventsCount = 0;
		long sumExtremeAggEventsCount = 0;
		long sumModerateAggEventsCount = 0;
		long sumNoneAggEventsCount = 0;
		
		for (AggregatedEvent event : driveResults.getAggregatedEvents()) {
			switch (event.intensity) {
			case EXCEED_THRESHOLD:
				sumExceedAggEventsCount++;
				break;
			case EXTREME:
				sumExtremeAggEventsCount++;
				break;
			case MODERATE:
				sumModerateAggEventsCount++;
				break;
			case NONE:
				sumNoneAggEventsCount++;
				break;
			}
		}
		
		File summaryFile = new File(makeDriveDirIfNotExist(driveRef), "summary.txt");
		PrintStream stream = new PrintStream(summaryFile);
		try {
			stream.println(String.format("Name: %s", driveRef.getName()));
			stream.println(String.format("Score: %d", score));
			stream.println(String.format("User score: %d", userScore));
			stream.println(String.format("Duration: %1.2f", sessionDurationMilli / 1000f));
			stream.println(String.format("Exceed: %d, Extreme: %d, Moderate: %d", 
					sumExceedAggEventsCount, sumExtremeAggEventsCount, sumModerateAggEventsCount));
			
			TableFormatter resultFormatter = new TableFormatter(stream);
			resultFormatter
				.addColumn("Event ID", 10)
				.addColumn("Time from start", 18)
				.addColumn("Duration", 10)
				.addColumn("Classification", 15)
				.addColumn("Intensity", 15)
				.endColumns();
			
			List<AggregatedEvent> events = driveResults.getAggregatedEvents();
			for (int i = 0; i < events.size(); i++) {
				AggregatedEvent event = events.get(i);
				resultFormatter.printLine(
						i+1, //id
						(event.time - events.get(0).time) / 1000.0f, //time
						event.duration / 1000.0f, //duration
						event.classification,
						event.intensity);
			}
		}
		finally {
			stream.close();
		}
	}
	
	private void saveAggregatedEventsCSV(File file, List<AggregatedEvent> events) throws FileNotFoundException {
		PrintStream eventsStream = null;
		try {
			eventsStream = new PrintStream(file);
			
			String format = "%s,%s,%s,%s,%s,%s";
			
			eventsStream.println(
					String.format(
							format, 
							"x ratio", 
							"y ratio", 
							"intensity", 
							"time",
							"duration",
							"classification"
					)
			);
			
			for (AggregatedEvent event : events) {
				eventsStream.println(
						String.format(
								format, 
								event.xRatio, 
								event.yRatio, 
								event.intensity.ordinal(), 
								event.time,
								event.duration,
								event.classification
						)
				);
			}
		}
		finally {
			eventsStream.close();
		}
	}
	
	private void saveRawEventsCSV(File file, List<RawEvent> events) throws FileNotFoundException {
		PrintStream eventsStream = null;
		try {
			eventsStream = new PrintStream(file);
			
			String format = "%s,%s,%s,%s";
			
			eventsStream.println(
					String.format(
							format, 
							"x ratio", 
							"y ratio", 
							"intensity", 
							"time"
					)
			);
			
			for (RawEvent event : events) {
				eventsStream.println(
						String.format(
								format, 
								event.xRatio, 
								event.yRatio, 
								event.intensity.ordinal(), 
								event.time
						)
				);
			}
		}
		finally {
			eventsStream.close();
		}
	}
	
	public List<DriveRef> getAllDrives() {
		List<DriveRef> driveRefs = new LinkedList<DriveRef>();
		for (File driveDirectory : dbDirectory.listFiles()) {
			if (driveDirectory.isDirectory()) {
				driveRefs.add(new DriveRef(driveDirectory.getName()));
			}
		}
		
		return driveRefs;
	}

	public DriveSamples loadDrive(DriveRef driveRef) throws Exception {
//		File driveDirectory = null;
//		for (File directory : dbDirectory.listFiles()) {
//			if (directory.getName().equals(driveRef.getName())) {
//				driveDirectory = directory;
//				break;
//			}
//		}
		
		File driveDirectory = new File(dbDirectory, driveRef.getName());
		if (!driveDirectory.exists()) { //no such drive
			return null;
		}
		
		DriveSamples drive = new DriveSamples();
		
		//load accelerometer and magnetic samples
		for (SampleType type : SampleType.values()) {
			File rawFile = new File(driveDirectory, type.seriFileName());
			drive.setSamples(Persistance.loadDriveRaw(rawFile), type);
		}

		driveRef.setDrive(drive);
				
		return drive;
	}
	
	@SuppressWarnings("unchecked")
	private static List<Sample> loadDriveRaw(File file) throws Exception {
		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(new FileInputStream(file));
			return (List<Sample>)in.readObject();
		}
		finally {
			in.close();
		}
	}
	
//	private static DriveSamples loadDriveCSV(File file) throws FileNotFoundException {
//		DriveSamples drive = new DriveSamples();
//		
//		Scanner scanner = null;
//		try {
//			scanner = new Scanner(new BufferedReader(new FileReader(file)));
//			scanner.useDelimiter(",|\n");
//			scanner.nextLine();
//			
//			while (scanner.hasNextLine() && scanner.hasNextDouble()) {
//				double x = scanner.nextDouble();
//				double y = scanner.nextDouble();
//				double z = scanner.nextDouble();
//				long timestamp = scanner.nextLong();
//				
//				drive.addSample(new Sample(x,y,z,timestamp));
//			}
//		}
//		finally {
//			scanner.close();
//		}
//		
//		return drive;
//	}
	
	public void deleteDrive(DriveRef driveRef) throws Exception {
		File fileDirectory = new File(dbDirectory, driveRef.getName());
		if (fileDirectory.exists()) {
			deleteRecursive(fileDirectory);
		}
		
		driveRef.setDrive(null);
	}
	
	private static void deleteRecursive(File file) {
		if (!file.isDirectory()) {
			file.delete();
		}
		else {
			for (File childFile : file.listFiles()) {
				deleteRecursive(childFile);
			}
			file.delete();
		}
	}

}
